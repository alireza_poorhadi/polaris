<?php
defined('BASE_PATH') or die("Permission Denied.");

function getYearGoals($whichYear = '')
{
    global $conn;
    global $currentYear;
    $cuttentUserId = getCurrentUserId();
    $whichYear = ($whichYear == '') ? $currentYear : $whichYear;
    $sql = "SELECT * FROM years_goals WHERE user_id = $cuttentUserId AND which_year = :which_year";
    $stmt = $conn->prepare($sql);
    $stmt->execute(['which_year' => $whichYear]);
    $rows = $stmt->fetchAll(PDO::FETCH_OBJ);
    return $rows;
}

function deleteYearGoal($yearGoalId)
{
    global $conn;
    $sql = "DELETE FROM years_goals WHERE id = $yearGoalId";
    $stmt = $conn->prepare($sql);
    $stmt->execute();
}

function changeYearGoalStatus($yearGoalId)
{
    global $conn;
    $cuttentUserId = getCurrentUserId();
    $sql = "UPDATE years_goals SET is_done = 1 - is_done WHERE user_id = :userId AND id = :yearGoalId";
    $stmt = $conn->prepare($sql);
    $stmt->execute(["userId" => $cuttentUserId, "yearGoalId" => $yearGoalId]);
    $selectSql = "SELECT is_done FROM years_goals WHERE id = :yearGoalId AND user_id = :userId";
    $stmt2 = $conn->prepare($selectSql);
    $stmt2->execute(["userId" => $cuttentUserId, "yearGoalId" => $yearGoalId]);
    $row = $stmt2->fetch(PDO::FETCH_OBJ);
    return $row->is_done;
}

function addNewGoal($taskTitle, $dateSelect)
{
    global $conn;
    $cuttentUserId = getCurrentUserId();
    $sql = "INSERT INTO years_goals (content, user_id, which_year) VALUES (:content,:user_id, :which_year)";
    $stmt = $conn->prepare($sql);
    $stmt->execute(['content' => $taskTitle, 'user_id' => $cuttentUserId, 'which_year' => $dateSelect]);
    $lastInsertId = $conn->lastInsertId();
    $sql2 = "SELECT * FROM years_goals WHERE id = $lastInsertId";
    $stmt2 = $conn->prepare($sql2);
    $stmt2->execute();
    $row = $stmt2->fetch(PDO::FETCH_OBJ);
    return $row;
}

function getMonthGoals($whichMonth, $folderId)
{
    global $conn;
    $folderCondition = '';
    if (is_numeric($folderId)) {
        $folderCondition = " AND folder_id = $folderId";
    }
    $cuttentUserId = getCurrentUserId();
    $sql = "SELECT * FROM months_goals WHERE user_id = $cuttentUserId AND which_month = '$whichMonth' $folderCondition";
    $stmt = $conn->prepare($sql);
    $stmt->execute();
    $rows = $stmt->fetchAll(PDO::FETCH_OBJ);
    return $rows;
}

function getDecisions($whichMonth, $folderId)
{
    global $conn;
    $folderCondition = '';
    if (is_numeric($folderId)) {
        $folderCondition = " AND folder_id = $folderId";
    }
    $cuttentUserId = getCurrentUserId();
    $sql = "SELECT * FROM decisions WHERE user_id = $cuttentUserId AND which_month = '$whichMonth' $folderCondition";
    $stmt = $conn->prepare($sql);
    $stmt->execute();
    $rows = $stmt->fetchAll(PDO::FETCH_OBJ);
    return $rows;
}

function getAssessments($whichMonth, $folderId)
{
    global $conn;
    $folderCondition = '';
    if (is_numeric($folderId)) {
        $folderCondition = " AND folder_id = $folderId";
    }
    $cuttentUserId = getCurrentUserId();
    $sql = "SELECT * FROM assessments WHERE user_id = $cuttentUserId AND which_month = '$whichMonth' $folderCondition";
    $stmt = $conn->prepare($sql);
    $stmt->execute();
    $rows = $stmt->fetchAll(PDO::FETCH_OBJ);
    return $rows;
}

function addMonthGoal($newMonthGoal, $whichMonth, $folderId)
{
    global $conn;
    $cuttentUserId = getCurrentUserId();
    $sql = "INSERT INTO months_goals (content, user_id, which_month, folder_id) VALUES (:content,:user_id, :which_month, :folder_id)";
    $stmt = $conn->prepare($sql);
    $stmt->execute(['content' => $newMonthGoal, 'user_id' => $cuttentUserId, 'which_month' => $whichMonth, 'folder_id' => $folderId]);
    $lastInsertId = $conn->lastInsertId();
    $sql2 = "SELECT * FROM months_goals WHERE id = :lastInsertId AND folder_id = :folderId AND which_month = :which_month";
    $stmt2 = $conn->prepare($sql2);
    $stmt2->execute(['lastInsertId' => $lastInsertId, 'which_month' => $whichMonth, 'folderId' => $folderId]);
    $row = $stmt2->fetch(PDO::FETCH_OBJ);
    return $row;
}

function addDecision($newDecision, $whichMonth, $folderId)
{
    global $conn;
    $cuttentUserId = getCurrentUserId();
    $sql = "INSERT INTO decisions (content, user_id, which_month, folder_id) VALUES (:content,:user_id, :which_month, :folder_id)";
    $stmt = $conn->prepare($sql);
    $stmt->execute(['content' => $newDecision, 'user_id' => $cuttentUserId, 'which_month' => $whichMonth, 'folder_id' => $folderId]);
    $lastInsertId = $conn->lastInsertId();
    $sql2 = "SELECT * FROM decisions WHERE id = :lastInsertId AND folder_id = :folderId AND which_month = :which_month";
    $stmt2 = $conn->prepare($sql2);
    $stmt2->execute(['lastInsertId' => $lastInsertId, 'which_month' => $whichMonth, 'folderId' => $folderId]);
    $row = $stmt2->fetch(PDO::FETCH_OBJ);
    return $row;
}

function addAssessment($newAssessment, $whichMonth, $folderId)
{
    global $conn;
    $cuttentUserId = getCurrentUserId();
    $sql = "INSERT INTO assessments (content, user_id, which_month, folder_id) VALUES (:content,:user_id, :which_month, :folder_id)";
    $stmt = $conn->prepare($sql);
    $stmt->execute(['content' => $newAssessment, 'user_id' => $cuttentUserId, 'which_month' => $whichMonth, 'folder_id' => $folderId]);
    $lastInsertId = $conn->lastInsertId();
    $sql2 = "SELECT * FROM assessments WHERE id = :lastInsertId AND folder_id = :folderId AND which_month = :which_month";
    $stmt2 = $conn->prepare($sql2);
    $stmt2->execute(['lastInsertId' => $lastInsertId, 'which_month' => $whichMonth, 'folderId' => $folderId]);
    $row = $stmt2->fetch(PDO::FETCH_OBJ);
    return $row;
}

function deleteMonthGoal($monthGoalId)
{
    global $conn;
    $sql = "DELETE FROM months_goals WHERE id = $monthGoalId";
    $stmt = $conn->prepare($sql);
    $stmt->execute();
}

function deleteDecision($decisionId)
{
    global $conn;
    $sql = "DELETE FROM decisions WHERE id = $decisionId";
    $stmt = $conn->prepare($sql);
    $stmt->execute();
}

function deleteAssessment($assessmentId)
{
    global $conn;
    $sql = "DELETE FROM assessments WHERE id = $assessmentId";
    $stmt = $conn->prepare($sql);
    $stmt->execute();
}

function changeMonthGoalStatus($monthGoalId)
{
    global $conn;
    $cuttentUserId = getCurrentUserId();
    $sql = "UPDATE months_goals SET is_done = 1 - is_done WHERE user_id = :userId AND id = :monthGoalId";
    $stmt = $conn->prepare($sql);
    $stmt->execute(["userId" => $cuttentUserId, "monthGoalId" => $monthGoalId]);
    $selectSql = "SELECT is_done FROM months_goals WHERE id = :monthGoalId AND user_id = :userId";
    $stmt2 = $conn->prepare($selectSql);
    $stmt2->execute(["userId" => $cuttentUserId, "monthGoalId" => $monthGoalId]);
    $row = $stmt2->fetch(PDO::FETCH_OBJ);
    return $row->is_done;
}

function changeDecisionStatus($decisionId)
{
    global $conn;
    $cuttentUserId = getCurrentUserId();
    $sql = "UPDATE decisions SET is_done = 1 - is_done WHERE user_id = :userId AND id = :decisionId";
    $stmt = $conn->prepare($sql);
    $stmt->execute(["userId" => $cuttentUserId, "decisionId" => $decisionId]);
    $selectSql = "SELECT is_done FROM decisions WHERE id = :decisionId AND user_id = :userId";
    $stmt2 = $conn->prepare($selectSql);
    $stmt2->execute(["userId" => $cuttentUserId, "decisionId" => $decisionId]);
    $row = $stmt2->fetch(PDO::FETCH_OBJ);
    return $row->is_done;
}

function doSearchForGoals($searchEntry, $page) {
    global $numberOfTasks;
    global $conn;
    $tasksPerPage = TASKS_PER_PAGE;
    $start = ($page - 1 ) * $tasksPerPage;
    $searchEntry = str_ireplace(' ', '%', $searchEntry);
    $cuttentUserId = getCurrentUserId();
    $sql = "SELECT * FROM months_goals WHERE user_id = :user_id AND content LIKE :content LIMIT $start, $tasksPerPage";
    $stmt = $conn->prepare($sql);
    $stmt->execute(['user_id' => $cuttentUserId, 'content' => '%'.$searchEntry.'%']);
    $rows = $stmt->fetchAll(PDO::FETCH_OBJ);
    $sqlFoundTasksNo = "SELECT COUNT(*) AS C FROM months_goals WHERE user_id = :user_id AND content LIKE :content";
    $stmt2 = $conn->prepare($sqlFoundTasksNo);
    $stmt2->execute(['user_id' => $cuttentUserId, 'content' => '%'.$searchEntry.'%']);
    $numberOfTasks = $stmt2->fetchAll(PDO::FETCH_OBJ)[0]->C;
    $rows['numberOfPages'] = $numberOfTasks;
    return $rows;
}

function doSearchForDecisions($searchEntry, $page) {
    global $numberOfTasks;
    global $conn;
    $tasksPerPage = TASKS_PER_PAGE;
    $start = ($page - 1 ) * $tasksPerPage;
    $searchEntry = str_ireplace(' ', '%', $searchEntry);
    $cuttentUserId = getCurrentUserId();
    $sql = "SELECT * FROM decisions WHERE user_id = :user_id AND content LIKE :content LIMIT $start, $tasksPerPage";
    $stmt = $conn->prepare($sql);
    $stmt->execute(['user_id' => $cuttentUserId, 'content' => '%'.$searchEntry.'%']);
    $rows = $stmt->fetchAll(PDO::FETCH_OBJ);
    $sqlFoundTasksNo = "SELECT COUNT(*) AS C FROM decisions WHERE user_id = :user_id AND content LIKE :content";
    $stmt2 = $conn->prepare($sqlFoundTasksNo);
    $stmt2->execute(['user_id' => $cuttentUserId, 'content' => '%'.$searchEntry.'%']);
    $numberOfTasks = $stmt2->fetchAll(PDO::FETCH_OBJ)[0]->C;
    $rows['numberOfPages'] = $numberOfTasks;
    return $rows;
}

function doSearchForAssessments($searchEntry, $page) {
    global $numberOfTasks;
    global $conn;
    $tasksPerPage = TASKS_PER_PAGE;
    $start = ($page - 1 ) * $tasksPerPage;
    $searchEntry = str_ireplace(' ', '%', $searchEntry);
    $cuttentUserId = getCurrentUserId();
    $sql = "SELECT * FROM assessments WHERE user_id = :user_id AND content LIKE :content LIMIT $start, $tasksPerPage";
    $stmt = $conn->prepare($sql);
    $stmt->execute(['user_id' => $cuttentUserId, 'content' => '%'.$searchEntry.'%']);
    $rows = $stmt->fetchAll(PDO::FETCH_OBJ);
    $sqlFoundTasksNo = "SELECT COUNT(*) AS C FROM assessments WHERE user_id = :user_id AND content LIKE :content";
    $stmt2 = $conn->prepare($sqlFoundTasksNo);
    $stmt2->execute(['user_id' => $cuttentUserId, 'content' => '%'.$searchEntry.'%']);
    $numberOfTasks = $stmt2->fetchAll(PDO::FETCH_OBJ)[0]->C;
    $rows['numberOfPages'] = $numberOfTasks;
    return $rows;
}