<?php
defined('BASE_PATH') or die("Permission Denied.");

function diePage($msg)
{
    echo "<div style='width: 80%;background-color: #ffafaf;padding: 30px;margin: 30px auto;text-align: center;border-radius: 10px;color: #6c2121;border: 1px solid #761b1b; font-size: 20px'>$msg</div>";
    die();
}

function isAjaxRequest()
{
    if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
        return true;
    }
    return false;
}