<?php
defined('BASE_PATH') or die("Permission Denied.");

function getCurrentUserId()
{
    return $_SESSION['login']->id;
}

function getFolders()
{
    global $conn;
    $cuttentUserId = getCurrentUserId();
    $sql = "SELECT * FROM folders WHERE user_id = $cuttentUserId";
    $stmt = $conn->prepare($sql);
    $stmt->execute();
    $rows = $stmt->fetchAll(PDO::FETCH_OBJ);
    return $rows;
}

function deleteFolder($folderId)
{
    global $conn;
    $sql = "DELETE FROM folders WHERE id = $folderId";
    $stmt = $conn->prepare($sql);
    $stmt->execute();
}


function addNewFolder($folderName)
{
    global $conn;
    $cuttentUserId = getCurrentUserId();
    $sql = "INSERT INTO folders (name, user_id) VALUES (:name,:user_id)";
    $stmt = $conn->prepare($sql);
    $stmt->execute(['name' => $folderName, 'user_id' => $cuttentUserId]);
    $lastInsertId = $conn->lastInsertId();
    $sql2 = "SELECT * FROM folders WHERE id = $lastInsertId";
    $stmt2 = $conn->prepare($sql2);
    $stmt2->execute();
    $row = $stmt2->fetch(PDO::FETCH_OBJ);
    return $row;
}

function getTasks($d = 'today')
{
    global $conn;
    $cuttentUserId = getCurrentUserId();
    $folder = $_GET['folderId'] ?? null;
    $folderCondition = '';
    if (isset($folder) and is_numeric($folder)) {
        $folderCondition = " and folder_id = $folder";
    }
    if ($d == 'today') {
        $dateCondition = 'and CONVERT(created_at, DATE) = DATE_ADD(curdate(), INTERVAL 0 DAY)';
    } else if ($d == 'yesterday') {
        $dateCondition = 'and CONVERT(created_at, DATE) = DATE_ADD(curdate(), INTERVAL -1 DAY)';
    } else if ($d == 'tomorrow') {
        $dateCondition = 'and CONVERT(created_at, DATE) = DATE_ADD(curdate(), INTERVAL 1 DAY)';
    } else if ($d == 'in2days') {
        $dateCondition = 'and CONVERT(created_at, DATE) = DATE_ADD(curdate(), INTERVAL 2 DAY)';
    } else if ($d == 'in3days') {
        $dateCondition = 'and CONVERT(created_at, DATE) = DATE_ADD(curdate(), INTERVAL 3 DAY)';
    } else if ($d == 'in4days') {
        $dateCondition = 'and CONVERT(created_at, DATE) = DATE_ADD(curdate(), INTERVAL 4 DAY)';
    } else if ($d == 'in5days') {
        $dateCondition = 'and CONVERT(created_at, DATE) = DATE_ADD(curdate(), INTERVAL 5 DAY)';
    } else if ($d == 'in6days') {
        $dateCondition = 'and CONVERT(created_at, DATE) = DATE_ADD(curdate(), INTERVAL 6 DAY)';
    }
    $sql = "SELECT * FROM tasks WHERE user_id = $cuttentUserId $folderCondition $dateCondition";
    $stmt = $conn->prepare($sql);
    $stmt->execute();
    $rows = $stmt->fetchAll(PDO::FETCH_OBJ);
    return $rows;
}

function deleteTask($taskId)
{
    global $conn;
    $sql = "DELETE FROM tasks WHERE id = $taskId";
    $stmt = $conn->prepare($sql);
    $stmt->execute();
}

function addNewTask($taskTitle, $folderId, $dateSelect)
{
    global $conn;
    $cuttentUserId = getCurrentUserId();
    $date = new DateTime();
    $dateCondition = $date->format('Y-m-d H:i:s');
    if ($dateSelect == '1day') {
        $tomorrow = new DateTime('tomorrow');
        $dateCondition = $tomorrow->format('Y-m-d H:i:s');
    }
    if ($dateSelect == '2days') {
        $date = new DateTime();
        $date->add(new DateInterval('P2D'));
        $dateCondition = $date->format('Y-m-d H:i:s');
    }
    if ($dateSelect == '3days') {
        $date = new DateTime();
        $date->add(new DateInterval('P3D'));
        $dateCondition = $date->format('Y-m-d H:i:s');
    }
    if ($dateSelect == '4days') {
        $date = new DateTime();
        $date->add(new DateInterval('P4D'));
        $dateCondition = $date->format('Y-m-d H:i:s');
    }
    if ($dateSelect == '5days') {
        $date = new DateTime();
        $date->add(new DateInterval('P5D'));
        $dateCondition = $date->format('Y-m-d H:i:s');
    }
    if ($dateSelect == '6days') {
        $date = new DateTime();
        $date->add(new DateInterval('P6D'));
        $dateCondition = $date->format('Y-m-d H:i:s');
    }
    $sql = "INSERT INTO tasks (title, user_id, folder_id, created_at) VALUES (:title,:user_id, :folder_id, :created_at)";
    $stmt = $conn->prepare($sql);
    $stmt->execute(['title' => $taskTitle, 'user_id' => $cuttentUserId, 'folder_id' => $folderId, 'created_at' => $dateCondition]);
    $lastInsertId = $conn->lastInsertId();
    $sql2 = "SELECT * FROM tasks WHERE id = $lastInsertId";
    $stmt2 = $conn->prepare($sql2);
    $stmt2->execute();
    $row = $stmt2->fetch(PDO::FETCH_OBJ);
    return $row;
}

function updateTaskFromPL($taskId, $folderId, $dateSelect)
{
    global $conn;
    $cuttentUserId = getCurrentUserId();
    $date = new DateTime();
    $dateCondition = $date->format('Y-m-d H:i:s');
    if ($dateSelect == '1day') {
        $tomorrow = new DateTime('tomorrow');
        $dateCondition = $tomorrow->format('Y-m-d H:i:s');
    }
    if ($dateSelect == '2days') {
        $date = new DateTime();
        $date->add(new DateInterval('P2D'));
        $dateCondition = $date->format('Y-m-d H:i:s');
    }
    if ($dateSelect == '3days') {
        $date = new DateTime();
        $date->add(new DateInterval('P3D'));
        $dateCondition = $date->format('Y-m-d H:i:s');
    }
    if ($dateSelect == '4days') {
        $date = new DateTime();
        $date->add(new DateInterval('P4D'));
        $dateCondition = $date->format('Y-m-d H:i:s');
    }
    if ($dateSelect == '5days') {
        $date = new DateTime();
        $date->add(new DateInterval('P5D'));
        $dateCondition = $date->format('Y-m-d H:i:s');
    }
    if ($dateSelect == '6days') {
        $date = new DateTime();
        $date->add(new DateInterval('P6D'));
        $dateCondition = $date->format('Y-m-d H:i:s');
    }
    $sql = "UPDATE tasks SET folder_id = :folder_id, created_at = :created_at WHERE id = :id AND user_id = :user_id";
    $stmt = $conn->prepare($sql);
    $stmt->execute(['folder_id' => $folderId, 'user_id' => $cuttentUserId, 'id' => $taskId, 'created_at' => $dateCondition]);
}

function transferTasksToPL()
{
    global $conn;
    $cuttentUserId = getCurrentUserId();
    $today = new DateTime('today');
    $dateCondition = $today->format('Y-m-d H:i:s');
    $sql = "UPDATE tasks SET folder_id = 11, created_at = '1981-06-14 04:00:00' WHERE is_done = 0 AND user_id = $cuttentUserId AND created_at < '$dateCondition'";
    $conn->query($sql);
}

function changeTaskStatus($taskId)
{
    global $conn;
    $cuttentUserId = getCurrentUserId();
    $sql = "UPDATE tasks SET is_done = 1 - is_done WHERE user_id = :userId AND id = :taskId";
    $stmt = $conn->prepare($sql);
    $stmt->execute(["userId" => $cuttentUserId, "taskId" => $taskId]);
    $selectSql = "SELECT is_done FROM tasks WHERE id = :taskId AND user_id = :userId";
    $stmt2 = $conn->prepare($selectSql);
    $stmt2->execute(["userId" => $cuttentUserId, "taskId" => $taskId]);
    $row = $stmt2->fetch(PDO::FETCH_OBJ);
    return $row->is_done;
}

function doSearch($searchEntry, $page)
{
    global $numberOfTasks;
    global $conn;
    $tasksPerPage = TASKS_PER_PAGE;
    $start = ($page - 1) * $tasksPerPage;
    $searchEntry = str_ireplace(' ', '%', $searchEntry);
    $cuttentUserId = getCurrentUserId();
    $sql = "SELECT * FROM tasks WHERE user_id = :user_id AND title LIKE :title LIMIT $start, $tasksPerPage";
    $stmt = $conn->prepare($sql);
    $stmt->execute(['user_id' => $cuttentUserId, 'title' => '%' . $searchEntry . '%']);
    $rows = $stmt->fetchAll(PDO::FETCH_OBJ);
    $sqlFoundTasksNo = "SELECT COUNT(*) AS C FROM tasks WHERE user_id = :user_id AND title LIKE :title";
    $stmt2 = $conn->prepare($sqlFoundTasksNo);
    $stmt2->execute(['user_id' => $cuttentUserId, 'title' => '%' . $searchEntry . '%']);
    $numberOfTasks = $stmt2->fetchAll(PDO::FETCH_OBJ)[0]->C;
    $rows['numberOfPages'] = $numberOfTasks;
    return $rows;
}

function getPrimaryList()
{
    global $conn;
    $cuttentUserId = getCurrentUserId();
    $sql = "SELECT * FROM tasks WHERE user_id = $cuttentUserId AND created_at = '1981-06-14 04:00:00'";
    $stmt = $conn->prepare($sql);
    $stmt->execute();
    $rows = $stmt->fetchAll(PDO::FETCH_OBJ);
    return $rows;
}

function addNewTaskPL($taskTitlePL)
{
    global $conn;
    $cuttentUserId = getCurrentUserId();
    $folderId = 11;
    $created_at = '1981-06-14 04:00:00';
    $sql = "INSERT INTO tasks (title, user_id, folder_id, created_at) VALUES (:title,:user_id, :folder_id, :created_at)";
    $stmt = $conn->prepare($sql);
    $stmt->execute(['title' => $taskTitlePL, 'user_id' => $cuttentUserId, 'folder_id' => $folderId, 'created_at' => $created_at]);
    $lastInsertId = $conn->lastInsertId();
    $sql2 = "SELECT * FROM tasks WHERE id = $lastInsertId";
    $stmt2 = $conn->prepare($sql2);
    $stmt2->execute();
    $row = $stmt2->fetch(PDO::FETCH_OBJ);
    return $row;
}
