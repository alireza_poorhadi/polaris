<?php
defined('BASE_PATH') or die("Permission Denied.");

function isLoggedIn()
{
    //return isset($_SESSION['login']) ? true : false;
    if (isset($_SESSION['login'])) {
        return true;
    }
    if (isset($_COOKIE['username'])) {
        $user = getUserByEmail($_COOKIE['username']);
        if (is_null($user)) {
            return false;
        }
        $_SESSION['login'] = $user;
        $user->image = "https://www.gravatar.com/avatar/" . md5(strtolower(trim($user->email)));
        return true;
    }
    return false;
}

function doRegister($userData)
{
    $fullname = $userData['fullname'];
    $email = $userData['email'];
    $password = $userData['password'];

    if (!is_null(getUserByEmail($email))) {
        return false;
    }
    global $conn;
    $passwordHash = password_hash($password, PASSWORD_BCRYPT);
    $sql = "INSERT INTO users (fullname, email, password) VALUES (:fullname,:email, :password)";
    $stmt = $conn->prepare($sql);
    $stmt->execute(['fullname' => $fullname, 'email' => $email, 'password' => $passwordHash]);
    return $stmt->rowCount() ? true : false;
}

function getUserByEmail($email)
{
    global $conn;
    $sql = "SELECT * FROM users WHERE email = :email";
    $stmt = $conn->prepare($sql);
    $stmt->execute(['email' => $email]);
    $row = $stmt->fetchAll(PDO::FETCH_OBJ);
    return $row[0] ?? null;
}

function doLogin($email, $password, $rememberMe)
{
    $user = getUserByEmail($email);
    if (is_null($user)) {
        return false;
    }

    if (password_verify($password, $user->password)) {
        $user->image = "https://www.gravatar.com/avatar/" . md5(strtolower(trim($user->email)));
        $_SESSION['login'] = $user;
        if($rememberMe == 1) {
            setcookie('username', $user->email, time() + (30 * 24 * 60 * 60));
        } else if ($rememberMe == 0) {
            setcookie('username', $user->email, time() - 3600);
        }
        return true;
    }
    return false;
}

function doLogout()
{
    unset($_SESSION['login']);
    setcookie('username', '', time() - 3600);
}

function getUsername($id)
{
    global $conn;
    $sql = "SELECT fullname, email FROM users WHERE id = :id";
    $stmt = $conn->prepare($sql);
    $stmt->execute(['id' => $id]);
    $row = $stmt->fetchAll(PDO::FETCH_OBJ);
    return $row[0] ?? null;
}

function changeUsername($username)
{
    global $conn;
    $cuttentUserId = getCurrentUserId();
    $sql = "UPDATE users SET fullname= :fullname WHERE id= :id";
    $stmt = $conn->prepare($sql);
    $stmt->execute(['fullname' => $username, 'id' => $cuttentUserId]);
}

function changePass($oldPass, $newPass)
{
    $cuttentUserId = getCurrentUserId();
    $user = getUserById($cuttentUserId);
    if (!password_verify($oldPass, $user->password)) {
        echo 'رمز عبور وارد شده معتبر نمی‌باشد!';
    } else {
        global $conn;
        $passwordHash = password_hash($newPass, PASSWORD_BCRYPT);
        $sql = "UPDATE users SET password= :pass WHERE id= :id";
        $stmt = $conn->prepare($sql);
        $stmt->execute(['pass' => $passwordHash, 'id' => $cuttentUserId]);
    }
}

function getUserById($id)
{
    global $conn;
    $sql = "SELECT * FROM users WHERE id = :id";
    $stmt = $conn->prepare($sql);
    $stmt->execute(['id' => $id]);
    $row = $stmt->fetchAll(PDO::FETCH_OBJ);
    return $row[0] ?? null;
}

function getAllUsers()
{
    global $conn;
    $sql = "SELECT * FROM users";
    $stmt = $conn->prepare($sql);
    $stmt->execute();
    $row = $stmt->fetchAll(PDO::FETCH_OBJ);
    return $row ?? null;
}

function getUsersNumbers()
{
    global $conn;
    $sql = "SELECT COUNT(*) AS c FROM users";
    $stmt = $conn->prepare($sql);
    $stmt->execute();
    $row = $stmt->fetchAll(PDO::FETCH_OBJ);
    return $row ?? null;
}
