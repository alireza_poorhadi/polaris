<?php
defined('BASE_PATH') or die("Permission Denied.");

function getGoal($yearGoalId)
{
    global $conn;
    global $currentYear;
    $cuttentUserId = getCurrentUserId();
    $sql = "SELECT * FROM years_goals WHERE user_id = $cuttentUserId AND id = :yearGoalId";
    $stmt = $conn->prepare($sql);
    $stmt->execute(['yearGoalId' => $yearGoalId]);
    $rows = $stmt->fetchAll(PDO::FETCH_OBJ);
    return $rows[0] ?? null;
}

function getBahas($yearGoalId)
{
    global $conn;
    global $currentYear;
    $cuttentUserId = getCurrentUserId();
    $sql = "SELECT * FROM baha_yeargoal WHERE user_id = $cuttentUserId AND yearGoal_id = :yearGoalId";
    $stmt = $conn->prepare($sql);
    $stmt->execute(['yearGoalId' => $yearGoalId]);
    $rows = $stmt->fetchAll(PDO::FETCH_OBJ);
    return $rows ?? null;
}

function getPReasons($yearGoalId)
{
    global $conn;
    global $currentYear;
    $cuttentUserId = getCurrentUserId();
    $sql = "SELECT * FROM reasons_yeargoal WHERE user_id = $cuttentUserId AND yearGoal_id = :yearGoalId AND type = 1";
    $stmt = $conn->prepare($sql);
    $stmt->execute(['yearGoalId' => $yearGoalId]);
    $rows = $stmt->fetchAll(PDO::FETCH_OBJ);
    return $rows ?? null;
}

function getNReasons($yearGoalId)
{
    global $conn;
    global $currentYear;
    $cuttentUserId = getCurrentUserId();
    $sql = "SELECT * FROM reasons_yeargoal WHERE user_id = $cuttentUserId AND yearGoal_id = :yearGoalId AND type = 0";
    $stmt = $conn->prepare($sql);
    $stmt->execute(['yearGoalId' => $yearGoalId]);
    $rows = $stmt->fetchAll(PDO::FETCH_OBJ);
    return $rows ?? null;
}

function addNewYearBaha($newYearBaha, $yearGoalId)
{
    global $conn;
    $cuttentUserId = getCurrentUserId();
    $sql = "INSERT INTO baha_yeargoal (content, user_id, yearGoal_id) VALUES (:content,:user_id, :yearGoal_id)";
    $stmt = $conn->prepare($sql);
    $stmt->execute(['content' => $newYearBaha, 'user_id' => $cuttentUserId, 'yearGoal_id' => $yearGoalId]);
    $lastInsertId = $conn->lastInsertId();
    $sql2 = "SELECT * FROM baha_yeargoal WHERE id = $lastInsertId";
    $stmt2 = $conn->prepare($sql2);
    $stmt2->execute();
    $row = $stmt2->fetch(PDO::FETCH_OBJ);
    return $row;
}

function addNewYearNReason($newYearNReason, $yearGoalId)
{
    global $conn;
    $cuttentUserId = getCurrentUserId();
    $sql = "INSERT INTO reasons_yeargoal (content, user_id, yearGoal_id, type) VALUES (:content,:user_id, :yearGoal_id, :type)";
    $stmt = $conn->prepare($sql);
    $stmt->execute(['content' => $newYearNReason, 'user_id' => $cuttentUserId, 'yearGoal_id' => $yearGoalId, 'type' => 0]);
    $lastInsertId = $conn->lastInsertId();
    $sql2 = "SELECT * FROM reasons_yeargoal WHERE id = $lastInsertId";
    $stmt2 = $conn->prepare($sql2);
    $stmt2->execute();
    $row = $stmt2->fetch(PDO::FETCH_OBJ);
    return $row;
}

function addNewYearPReason($newYearPReason, $yearGoalId)
{
    global $conn;
    $cuttentUserId = getCurrentUserId();
    $sql = "INSERT INTO reasons_yeargoal (content, user_id, yearGoal_id, type) VALUES (:content,:user_id, :yearGoal_id, :type)";
    $stmt = $conn->prepare($sql);
    $stmt->execute(['content' => $newYearPReason, 'user_id' => $cuttentUserId, 'yearGoal_id' => $yearGoalId, 'type' => 1]);
    $lastInsertId = $conn->lastInsertId();
    $sql2 = "SELECT * FROM reasons_yeargoal WHERE id = $lastInsertId";
    $stmt2 = $conn->prepare($sql2);
    $stmt2->execute();
    $row = $stmt2->fetch(PDO::FETCH_OBJ);
    return $row;
}

function deleteYearBaha($yearBahaId)
{
    global $conn;
    $sql = "DELETE FROM baha_yeargoal WHERE id = $yearBahaId";
    $stmt = $conn->prepare($sql);
    $stmt->execute();
}

function deleteYearNReason($yearNReasonId)
{
    global $conn;
    $sql = "DELETE FROM reasons_yeargoal WHERE id = $yearNReasonId";
    $stmt = $conn->prepare($sql);
    $stmt->execute();
}

function deleteYearPReason($yearPReasonId)
{
    global $conn;
    $sql = "DELETE FROM reasons_yeargoal WHERE id = $yearPReasonId";
    $stmt = $conn->prepare($sql);
    $stmt->execute();
}
