<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="<?= BASE_URL ?>assets/img/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="<?= BASE_URL ?>assets/css/style1.css">
    <title><?= SITE_TITLE ?></title>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
</head>

<body onload="document.getElementById('inputPL').focus();" style="margin: 0;">
    <a href="<?= BASE_URL ?>" class="returnM">بازگشت</a>
    <div id="bg">
        <div id="containerPL">

            <div id="guide">در این صفحه، می‌توانید فعالیت‌هایی را وارد کنید، که هنوز نمی‌دانید که چه روزی از هفته پیش رو قرار است انجام دهید و یا صرفاً قصد دارید که آنها را فراموش نکنید. بعداً می‌توانید آنها را به برنامه‌های هفته پیش رو (با استفاده از ابزارهای همین صفحه) اضافه کنید. همچنین تمام برنامه‌هایی را که امروز موفق به انجام آن نشده‌اید و تیک نخورده‌اند، در روز بعد به طور خودکار به این صفحه منتقل می‌شوند تا بتوانید آنها را دوباره مدیریت کنید.</div>
            <br>
            <div style="text-align: center;">
                <a href="<?= BASE_URL ?>" class="return">بازگشت</a>
            </div>

            <h2>لیست اولیه</h2>
            <hr>
            <br>
            <input type="text" placeholder="افزودن برنامه جدید" id="inputPL">
            <br><br>

            <ol>

                <?php if (sizeof($primaryLists) > 0) : ?>
                    <?php foreach ($primaryLists as $primaryList) : ?>
                        <li id="<?= $primaryList->id ?>">
                            <span><?= $primaryList->title ?></span>
                            <br>
                            <span class="remove" id="<?= $primaryList->id ?>" title="حذف">
                                <img src="<?= BASE_URL ?>assets/img/trash.svg" width="16">
                            </span>
                            <div id="divInsideList">

                                <br><br>
                                <div>
                                    <span style="color: gray;">افزودن به: </span>
                                    <input type="checkbox" id="emrooz" name="emrooz" value="emrooz" class="days"><label for="emrooz" class="daysLabel">امروز</label>
                                    <input type="checkbox" id="1day" name="1day" value="1day" class="days"><label for="1day" class="daysLabel">فردا</label>
                                    <input type="checkbox" id="2days" name="2days" value="2days" class="days"><label for="2days" class="daysLabel"><?= $v->addDay(2)->format('l j F') ?></label>
                                    <input type="checkbox" id="3days" name="3days" value="3days" class="days"><label for="3days" class="daysLabel"><?= $v->addDay()->format('l j F') ?></label>
                                    <input type="checkbox" id="4days" name="4days" value="4days" class="days"><label for="4days" class="daysLabel"><?= $v->addDay()->format('l j F') ?></label>
                                    <input type="checkbox" id="5days" name="5days" value="5days" class="days"><label for="5days" class="daysLabel"><?= $v->addDay()->format('l j F') ?></label>
                                    <input type="checkbox" id="6days" name="6days" value="6days" class="days"><label for="6days" class="daysLabel"><?= $v->addDay()->format('l j F') ?></label>
                                    <br><br>
                                    <span style="color: gray;">دسته‌بندی: </span>
                                    <select name="" id="selectPL">
                                        <?php foreach ($folders as $folder) : ?>
                                            <option value="<?= $folder->id ?>"><?= $folder->name; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                    <button class="addNewBtn">برو</button>
                                </div>
                            </div>
                        </li>
                        <?php $v->addDay(-6) ?>
                    <?php endforeach; ?>
                <?php else : ?>
                    <li>
                        <span style="color: #d05151;">
                            هنوز برنامه‌ای وجود ندارد.
                        </span>
                    </li>
                <?php endif; ?>
            </ol>
            <br>
            <div style="text-align: center;">
                <a href="<?= BASE_URL ?>" class="return">بازگشت</a>
            </div>


            <script src="<?= BASE_URL ?>assets/js/jquery-3.5.1.min.js"></script>

            <script>
                $(document).ready(function() {

                    $('#inputPL').keydown(function(e) {
                        if (e.keyCode === 13) {
                            var newTaskPL = $('#inputPL').val();
                            $.ajax({
                                url: "process/ajaxHandler.php",
                                type: 'post',
                                data: {
                                    action: "addTaskPL",
                                    newTaskPL: newTaskPL
                                },
                                success: function(response) {
                                    if (/^[\],:{}\s]*$/.test(response.replace(/\\["\\\/bfnrtu]/g, '@').replace(/"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g, ']').replace(/(?:^|:|,)(?:\s*\[)+/g, ''))) {
                                        location.reload();
                                    } else {
                                        swal({
                                            title: 'توجه',
                                            text: response,
                                            icon: "error",
                                            className: 'bb',
                                            button: {
                                                text: 'باشه',
                                                className: 'sab'
                                            },
                                        });
                                    }
                                }
                            });
                        }
                    });

                    $('div').on('click', '.remove', function(e) {
                        var obj = $(this);
                        var olObj = obj.parents('ol');
                        var parentObjId = obj.parent().attr('id');
                        swal({
                                title: 'توجه',
                                text: 'آیا مطمئن هستید؟',
                                icon: 'warning',
                                className: "DBox",
                                buttons: {
                                    cancel: {
                                        visible: true,
                                        text: "لغو",
                                        className: 'cancelb'
                                    },
                                    confirm: {
                                        text: 'تایید',
                                        className: 'confirmb'
                                    }
                                },
                                dangerMode: true,
                            })
                            .then((willDelete) => {
                                if (willDelete) {

                                    $.ajax({
                                        url: "process/ajaxHandler.php",
                                        type: 'post',
                                        data: {
                                            action: "deleteTask",
                                            taskId: parentObjId
                                        },
                                        success: function(response) {
                                            obj.parent().remove();
                                            noProgram(olObj);
                                        }
                                    });
                                }
                            });
                    });

                    function noProgram(obj) {
                        if (obj.text().trim() == '') {
                            obj.html('<li style="color: #d05151;">هنوز برنامه‌ای وجود ندارد.</li>');
                        }
                    }

                    $('.addNewBtn').click(function() {
                        var folderId = $(this).parents('ol').find('#selectPL').val();
                        var taskId = $(this).parents('li').attr('id');
                        var taskTitle = $(this).parents('li').find("span:first-child").html().trim();
                        var emroozCheckbox = $(this).parents('#divInsideList').find('#emrooz');
                        var oneDayCheckbox = $(this).parents('#divInsideList').find('#1day');
                        var twoDaysCheckbox = $(this).parents('#divInsideList').find('#2days');
                        var threeDaysCheckbox = $(this).parents('#divInsideList').find('#3days');
                        var fourDaysCheckbox = $(this).parents('#divInsideList').find('#4days');
                        var fiveDaysCheckbox = $(this).parents('#divInsideList').find('#5days');
                        var sixDaysCheckbox = $(this).parents('#divInsideList').find('#6days');
                        var whatDay = ['none'];
                        if (emroozCheckbox.is(":checked")) {
                            whatDay.push(emroozCheckbox.val());
                        }

                        if (oneDayCheckbox.is(":checked")) {
                            whatDay.push(oneDayCheckbox.val());
                        }

                        if (twoDaysCheckbox.is(":checked")) {
                            whatDay.push(twoDaysCheckbox.val());
                        }

                        if (threeDaysCheckbox.is(":checked")) {
                            whatDay.push(threeDaysCheckbox.val());
                        }

                        if (fourDaysCheckbox.is(":checked")) {
                            whatDay.push(fourDaysCheckbox.val());
                        }

                        if (fiveDaysCheckbox.is(":checked")) {
                            whatDay.push(fiveDaysCheckbox.val());
                        }

                        if (sixDaysCheckbox.is(":checked")) {
                            whatDay.push(sixDaysCheckbox.val());
                        }

                        if (whatDay.length > 1) {
                            whatDay.shift();
                        }

                        $.ajax({
                            url: "process/ajaxHandler.php",
                            type: 'post',
                            data: {
                                action: "addTaskFromPL",
                                dateSelects: whatDay,
                                taskId: taskId,
                                folderId: folderId,
                                taskTitle: taskTitle
                            },
                            success: function(response) {
                                if (response != 'OK') {
                                    swal({
                                        title: 'توجه',
                                        text: response,
                                        icon: "error",
                                        className: 'bb',
                                        button: {
                                            text: 'باشه',
                                            className: 'sab'
                                        },
                                    });
                                } else {
                                    location.reload();
                                }

                            }
                        });

                    });
                });
            </script>
        </div>
</body>

</html>