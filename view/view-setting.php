<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="<?= BASE_URL ?>assets/img/favicon.ico" type="image/x-icon">
    <link href="<?= BASE_URL . 'assets/css/setting1.css' ?>" rel="stylesheet">
    <script src="<?=BASE_URL?>assets/js/jquery-3.5.1.min.js"></script>
    <title>تنظیمات پولاریس</title>
</head>

<body> 
    <div id="container">
        <section>
            <h2>تنظیمات</h2>
            <hr>
            <div>
                <h3>نام نمایشی</h3>
                <input type="text" id="username" value="<?= $username ?>">
            </div>

            <div>
                <h3>رمز عبور</h3>
                <input type="password" id="oldPass" placeholder="رمز عبور فعلی">
                <input type="password" id="newPass" placeholder="رمز عبور جدید">
                <input type="password" id="reNewPass" placeholder="تکرار رمز عبور جدید">
            </div>
            <button id="save">ذخیره</button>
            <a href="<?= BASE_URL ?>">انصراف</a>

        </section>
        <section id="about">
            <br><br>
            <h3>درباره ...</h3>
            <hr>
            <ul>
                <li>پولاریس: سیستم مدیریت اهداف و برنامه‌های روزانه</li>
                <li>نسخه اپلیکیشن: 1.2</li>
                <li>برنامه‌نویس: <a href="https://www.instagram.com/alireza.poorhadi/">علیرضا پورهادی</a></li>
                <li>تاریخ انتشار: 15 آبان 1399</li>
            </ul>

            <hr>
            <h3>تغییرات نسخه 1.1</h3>
            <ul>
                <li>اضافه شدن صفحه‌ای تحت عنوان «لیست اولیه» برای لیست کردن برنامه‌هایی که فعلاً زمان خاصی برای آنها در نظر ندارید و یا صرفاً نمی‌خواهید آنها را فراموش کنید. با ابزار موجود در این صفحه می‌توانید هر زمان، برنامه مد نظر را به روز معینی اضافه کنید.
                    همچنین برنامه‌هایی که احیاناً در روز مقرر انجام نشده‌ و در برنامه تیک نخورده‌اند، به طور خودکار به «لیست اولیه» اضافه می‌شوند تا بعداً مدیریت شوند.</li>
                <li>در صفحه «برنامه‌ها» اکنون می‌توانید برنامه مد نظر خود را به بیش از یک روز اضافه کنید.</li>

            </ul>

            <h3>تغییرات نسخه 1.2</h3>

            <ul>
                <li>
                تغییرات اساسی در ظاهر و عملکرد پولاریس
                </li>
            </ul>


        </section>
        <br>
        <hr>
        <?php if ($userEmail == "alireza_pourhadi@yahoo.com") : ?>
            <section>
                <p>تعداد کل ثبت نام کنندگان در سایت = <?= getUsersNumbers()[0]->c; ?></p>
                <table>
                    <tr>
                        <td>شناسه</td>
                        <td>نام</td>
                        <td>ایمیل</td>
                        <td>تاریخ عضویت</td>
                    </tr>
                    <?php foreach ($allUsers as $allUser) : ?>
                        <tr>
                            <td><?= $allUser->id ?></td>
                            <td><?= $allUser->fullname ?></td>
                            <td><?= $allUser->email ?></td>
                            <td style="direction: ltr;"><?= $allUser->created_at ?></td>
                        </tr>

                    <?php endforeach; ?>
                </table>
            </section>
        <?php endif; ?>
    </div>

    <script>
        $(document).ready(function() {

            $('#username').keydown(function(e) {
                if (e.keyCode === 13) {
                    $('#oldPass').focus();
                }
            })
            $('#oldPass').keydown(function(e) {
                if (e.keyCode === 13) {
                    $('#newPass').focus();
                }
            })

            $('#newPass').keydown(function(e) {
                if (e.keyCode === 13) {
                    $('#reNewPass').focus();
                }
            })

            $('#reNewPass').keydown(function(e) {
                if (e.keyCode === 13) {
                    sendAjaxRequest();
                }
            })

            $('#save').click(function(e) {
                sendAjaxRequest();
            })

            function sendAjaxRequest() {
                var newUsername = $('#username').val();
                var oldPass = $('#oldPass').val();
                var newPass = $('#newPass').val();
                var reNewPass = $('#reNewPass').val();
                $.ajax({
                    url: "process/settingAjax.php",
                    type: 'post',
                    data: {
                        newUsername: newUsername,
                        oldPass: oldPass,
                        newPass: newPass,
                        reNewPass: reNewPass
                    },
                    success: function(response) {
                        if (response.length <= 0) {
                            swal({
                                title: 'توجه',
                                text: 'تغییرات با موفقیت ذخیره شد.',
                                icon: "success",
                                className: 'bb',
                                button: false
                            });
                            setTimeout(function() {
                                location.href = "<?= BASE_URL ?>";
                            }, 2000)

                        } else {
                            swal({
                                title: 'توجه',
                                text: response,
                                icon: "error",
                                className: 'bb',
                                button: {
                                    text: 'باشه',
                                    className: 'sab'
                                },
                            });
                        }
                    }
                });
            }
        });
    </script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
</body>

</html>