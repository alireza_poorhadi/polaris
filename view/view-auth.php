<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="<?= BASE_URL ?>assets/img/favicon.ico" type="image/x-icon">
    <link href="<?= BASE_URL . 'assets/css/auth1.css' ?>" rel="stylesheet">
    <script src="<?= BASE_URL ?>assets/js/jquery-3.5.1.min.js"></script>
    <title>ورود به پولاریس</title>
</head>

<body>
    <div id="container">
        <section class="accMenu">
            <div class="accTitles">
                <div class="accTitle defaultTab" data-tab="login">&nbsp;&nbsp;&nbsp;ورود&nbsp;&nbsp;&nbsp;</div>
                <div class="accTitle" data-tab="register">&nbsp;ثبت نام&nbsp;</div>

            </div>

            <div class="accContents">
                <div id="login" class="accContent default">
                    <div class="form-container sign-in-container">
                        <form action="<?= BASE_URL . 'auth.php?action=login' ?>" method="POST">
                            <div>
                                <img src="<?= BASE_URL ?>assets/img/polaris_logo.png" alt="پولاریس" width="175">
                            </div>
                            <div class="<?= $error ? 'error' : 'success';
                                        echo is_null($msg) ? ' no-show' : ' show' ?>"><?= $msg ?></div>
                            <input name="email" type="text" placeholder="ایمیل یا نام کاربری" required>
                            <input name="password" type="password" placeholder="رمز عبور" required>
                            <input type="checkbox" id="rememberMe" name="rememberMe" value="1">
                            <label for="rememberMe">مرا به خاطر بسپار.</label>
                            <button>ورود</button>
                        </form>
                    </div>
                </div>

                <div id="register" class="accContent">
                    <div class="form-container sign-up-container">
                        <form action="<?= BASE_URL . 'auth.php?action=register' ?>" method="POST" id="registerForm">

                            <div>
                                <img src="<?= BASE_URL ?>assets/img/polaris_logo.png" alt="پولاریس" width="175">
                            </div>
                            <h2>ایجاد حساب کاربری</h2>

                            <input name="fullname" type="text" placeholder="نام" required>
                            <input name="email" id="email" type="text" placeholder="ایمیل یا نام کاربری (به صورت لاتین)" required>
                            <span id="isFree"></span>
                            <input name="password" type="password" id="password" placeholder="رمز عبور" required>
                            <span id="isValidPass"></span>
                            <button>ثبت‌نام</button>
                        </form>
                    </div>

                </div>


            </div>

        </section>
    </div>
    <script>
        $(document).ready(function() {
            $('#email').change(function() {
                var email = $(this).val();
                $.ajax({
                    url: "process/ajaxHandler.php",
                    type: 'post',
                    data: {
                        action: "checkEmailExistence",
                        email: email
                    },
                    success: function(response) {
                        if (response) {
                            $('#isFree').html('<span class="success">ایمیل یا نام کاربری در دسترس</span>');
                        } else {
                            $('#isFree').html('<span class="error">این ایمیل یا نام کاربری در سامانه موجود می‌باشد</span>');
                        }
                    }
                });
            });

            $('.accTitle').click(function() {
                $('.default').css('display', 'none');
                $('.accContent').hide();
                $('.accTitle').css({
                    backgroundColor: '#ddd'
                });
                if ($('#' + $(this).attr('data-tab')).css('display') === 'none') {
                    $('#' + $(this).attr('data-tab')).fadeIn();
                    $(this).css({
                        backgroundColor: '#fafafa',
                        borderBottom: '1px solid #fafafa'
                    });
                }
            });

        });
    </script>
</body>

</html>