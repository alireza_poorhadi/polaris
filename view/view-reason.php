<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="<?= BASE_URL ?>assets/img/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="<?= BASE_URL ?>assets/css/baha1.css">
    <title><?= SITE_TITLE ?></title>
</head>

<body>
    <div id="container">
        <div id="guide">در این قسمت می‌توانید دلیل و یا دلایل خود را برای رسیدن به هدف، قید کنید.</div>
        <a class="more">توضیحات بیشتر ...</a>
        <div id="moreExp" cite="https://abasmanesh.com/fa/">
            <p>شاید همه ما به یاد بیاریم زمانی را که کودک بودیم و سرشار از آرزوها … سرشار از درخواست …. آری! ما نامحدود بودیم … اما همه ما دارای آرزوهای برباد رفته و به ثمر ننشسته زیادی در کارنامه زندگیمان هستیم …. آرزوهایی که با شور سوزانی وجود ما را روشن کردند، اما حتی نمی‌توانیم بخاطر بیاوریم که چگونه آن شعله سوزان اشتیاق خاموش شد و ما هرگز به آرزوهایمان نرسیدیم … تا جایی که خیلی از ماها دیگر آرزو نکردیم … ما خالی از درخواست شدیم … ما در متن زندگی ماندیم و همراه ساعت‌ها و ثانیه‌ها شدیم …. ما به روزمرگی تن دادیم …..</p>

            <p>علت چه می‌تواند باشد؟ علت <span style="color: green;">همان اهرم رنج و لذتی </span>است که در ذهن ما تعریف شده است. اگه در طول روز به تمام کارهایی که انجام می‌دهیم نگاه کنیم، می‌بینیم که تمام آنها را می‌توانیم بر اساس این اهرم توجیه کنیم. تمام کارهایی که روزانه انجام می‌دهیم، برای فرار از رنجی است و برای تجربه یک لذت. به عنوان مثال آب خوردن را در نظر بگیرید. رنج تشنگی ما را وا می دارد که برای فرار از آن کاری انجام دهیم، به سمت شیر آب حرکت می کنیم و شروع به خوردن آب می کنیم، چرا که خوردن آن تجربه لذت بخشی است. تا کی به خوردن آب ادامه می دهیم؟ مسلماً تا زمانی که تبدیل به رنج نشود و از انجامش لذت ببریم، به محض اینکه ادامه دادن آن رنج آور شود، خوردن آب را متوقف می‌کنیم.</p>

            <p>هدف در ذهن ما آن کاریست که با نیروی اراده شروع شود و صبح و شب را به هم بدوزد و زجر را با تمام وجود احساس کند تا در نهایت موفق شود. یعنی همیشه در ذهن ما، بعد از هر بار هدف‌گذاری، اراده‌ای پولادین تعریف می‌شود که بشود با آن، ساعت‌ها و روزها و هفته‌ها را در نهایت سختی گذراند و علی رغم میل درونی، زندگی کرد و کار کرد و استراحت و تفریح و لبخند را تعطیل کرد، اراده‌ای که بتواند این‌همه مشقت را تحمل کند. ذهن ما می‌گوید رسیدن به هدفت ارزش این‌همه بدبختی و بیچارگی را ندارد …. و به همین خاطر است که تب‌های داغ و اشتیاق سوزان خیلی زود خشک می‌شوند و یا به اهدافمان نمی‌رسیم و یا اگر می‌رسیم، همیشه در مسیر اهداف، سرد و خاموش می‌شویم. در حالی که آرزو همان طور که از اسمش مشخص است باید انسان را سر شوق و به وجد بیاورد.</p>

            <p>بعد از هدف‌گذاری، به جای اراده پولادین برای دوختن صبح به شب ، باید شروع به ساختن باور و درست کردن اهرم‌های ذهنمان کنیم تا آن اهرم‌ها مسیر را برایمان روشن و لذت بخش کنند، تا آن اهرم‌ها پای رفتنمان شوند، تا با راهکارها و ایده‌ها و آدم‌های مناسب احاطه شویم، تا در مدار خواسته‌مان قرار بگیریم، نه اینکه تقلا کنیم یا با فشار جسم‌مان به خواسته‌هایمان برسیم.</p>

            <p>پس اگر رسیدن به اهداف مترادف با زجر کشیدن در ذهن تعریف شده باشد، از آنجا که ذهن از کارهای زجرآور فرار می‌کند، در نتیجه تمام قوایش را جمع می‌کند که اجازه ندهد فرد به هدفش برسد. چرا که رسیدن به آن هدف خاص برایش رنج آور و همراه با مشقت است. مثلاً اگر به دست آوردن پول را سخت بدانیم، ذهن، ما را همواره در شرایطی قرار می‌دهد که نتوانیم به خواسته‌مان برسیم، چرا که رسیدن به این خواسته برای ذهن، زجر آور تعریف شده است و برای فرار از این زجر طوری عمل می‌کند که فرد علی رغم تمام اراده و تلاشی که به خرج می‌دهد و علی رغم تمام فعالیت‌های فیزیکی، پولی بدست نیاورد و به عبارتی این اهرم اشتباه که به خاطر باورهای غلط و عملکرد اشتباه ذهن، ایجاد شده جلوی رسیدن به ثروت را می‌گیرد. چون فکر می‌کند به دست آوردن پول سخت است و همواره از آن سختی گریزان است و کاری که ما آگاهانه باید انجام بدهیم، پیدا کردن اهرم‌های اشتباه و درست کردن آنها با ایجاد باور درست و جابجا کردن آنهاست به نحوی که در این مثال، به دست آوردن پول را آسان بدانیم و برای ساختن اهرمی که بر این پایه ساخته شود تمام قوای ذهن را به کار بگیریم و آن وقت ما می‌توانیم بگوییم که با خواسته‌مان همسو و هم جهت شده‌ایم و بدون اینکه بخواهیم آگاهانه اراده پولادین خود را برای رسیدن به خواسته‌ها جزم کنیم و با اهرم اشتباه و زنگ زده از صبح تا شب کلنگ بزنیم، خودبخود در مداری قرار می‌گیریم که با ایده‌ها و راهکارها و فرصت‌ها و موقعیت‌ها و آدم‌هایی که می‌توانند دستان خداوند شوند، احاطه می‌شویم، در مداری که همه چیز مهیاست برای اینکه ما خواسته خود را تجربه کنیم. یعنی بدون هیچ گونه زجری و بدون دوختن صبح‌های زیبای زندگی به شبهای آرام مان می‌توانیم خیلی راحت آرزوی خود را بدست بیاوریم.</p>

            <p>زمانی که اهداف بسیاری در سر داریم ولی نمی توانیم به آنها برسیم و یا اراده لازم برای رسیدن به آنها را در خود نمی‌بینیم، و یا خود را فرد بسیار تنبلی می‌دانیم، <span style="color: red;">در واقع هیچ کدام از این موارد در مورد ما صادق نیست.</span> تنها چیزی که وجود دارد این است که اهرم رنج و لذت در ذهن ما برعکس شده است. باید کاری کرد که از نظر ذهن رسیدن به هدف فوق العاده لذت بخش باشد و نرسیدن به آن فوق العاده رنج آور. </p>

            <p style="background-color: lightgreen; padding: 0 7px; border-radius: 3px;">برای انجام این کار باید لیستی تهیه کرد از تمام رنج و زجرهایی ممکنی که با نرسیدن به هدف، آنها را تجربه خواهیم کرد و با تمام وجود آنها را حس کنیم و همچنین لیستی تهیه کنیم از تمام لذت‌هایی که با رسیدن به هدف و خواسته مان تجربه شان خواهیم کرد و نیز آنها را با تمام وجود حس کنیم. </p>

            <p style="color: green;"><strong>این قسمت می تواند شما را برای این موضوع راهنمایی کند.</strong></p>

            <p>بهتر است تا می توانید لیستی بلند بالا در هر مورد تهیه کنید و هر روز به لیستتان نگاه کنید و بخوانید و حس کنید. <span style="color: green;">با انجام همین کار ساده، معجزه را حتماً در زندگی خود شاهد خواهید بود.</span></p>

            <br>
            <p style="font-size: 16px;">منبع: <a href="https://abasmanesh.com/fa/" title="گروه تحقیقاتی عباس‌منش" target="_blank" style="color: blue;">گروه تحقیقاتی عباس‌منش</a></p>
            <br>
            <hr>
        </div>
        <h3><?= $goal->content; ?></h3>
        <section class="accMenu">
            <div class="accItem">
                <div class="accTitle" style="background-color: #f78a8a;">اگر به این خواسته و یا هدف خود نرسم، چه اتفاقات بد و ناخواسته‌ای برای من به وجود خواهد آمد؟</div>
                <div class="accContent">
                    <div id="nReasonInput">
                        <input type="text" placeholder="افزودن">
                    </div>

                    <ul id="nReasonList">
                        <?php if (sizeof($nReasons) > 0) : ?>
                            <?php foreach ($nReasons as $nReason) : ?>
                                <div id="<?= $nReason->id ?>">
                                    <li data-yearNReasonId="<?= $nReason->id ?>"><?= $nReason->content ?>
                                        <span class="remove" title="حذف" data-yearNReasonId="<?= $nReason->id ?>">
                                            <img src="<?= BASE_URL ?>assets/img/trash.svg" width="18">
                                        </span></li>
                                </div>
                            <?php endforeach; ?>
                        <?php else : ?>
                            <li style="color: #d05151; list-style-type: none;">موردی وجود ندارد.</li>
                        <?php endif; ?>
                    </ul>
                    <br><br>

                </div>
            </div>
            <div class="accItem">
                <div class="accTitle" style="background-color: #a2f1a2;">اگر به این خواسته و یا هدف خود برسم چه اتفاقات و شرایط خوشایند و لذت‌بخشی تجربه خواهم کرد؟</div>
                <div class="accContent">
                    <div id="pReasonInput">
                        <input type="text" placeholder="افزودن">
                    </div>

                    <ul id="pReasonList">
                        <?php if (sizeof($pReasons) > 0) : ?>
                            <?php foreach ($pReasons as $pReason) : ?>
                                <div id="<?= $pReason->id ?>">
                                    <li data-yearPReasonId="<?= $pReason->id ?>"><?= $pReason->content ?>
                                        <span class="remove" title="حذف" data-yearPReasonId="<?= $pReason->id ?>">
                                            <img src="<?= BASE_URL ?>assets/img/trash.svg" width="18">
                                        </span></li>
                                </div>
                            <?php endforeach; ?>
                        <?php else : ?>
                            <li style="color: #d05151; list-style-type: none;">
                                موردی وجود ندارد.
                            </li>
                        <?php endif; ?>
                    </ul>
                    <br><br>

                </div>
            </div>
        </section>
        <br><br>

        <div style="text-align: center;">
            <a href="<?= BASE_URL . 'goalsManager.php' ?>" id="return">بازگشت</a>
        </div>

    </div>
    <script src="<?=BASE_URL?>assets/js/jquery-3.5.1.min.js"></script>

    <script>
        $(document).ready(function() {
            $('.more').click(function() {
                $('#moreExp').slideToggle(1000);
                if ($('.more').text() == 'توضیحات بیشتر ...') {
                    $('.more').text('حذف توضیحات');
                } else {
                    $('.more').text('توضیحات بیشتر ...');
                }

            });

            $('.accTitle').click(function() {
                $('.accContent').slideUp();
                if ($(this).next('.accContent').css('display') == 'none')
                    $(this).next('.accContent').slideDown();
            });

            // adding new year negative reason (ajax)

            // $('#nReasonInput button').click(function(e) {
            //     e.preventDefault();
            //     addNewGoal();
            // });

            $('#nReasonInput input').keydown(function(e) {
                if (e.keyCode === 13) {
                    addNewNReason();
                }
            });

            function addNewNReason() {
                var newNReason = $('#nReasonInput input').val();
                $.ajax({
                    url: "process/ajaxHandler.php",
                    type: 'post',
                    data: {
                        action: "addNReason",
                        newYearNReason: newNReason,
                        yearGoalId: <?= isset($_GET['yearGoalId']) ? ($_GET['yearGoalId']) : null; ?>
                    },
                    success: function(response) {

                        if (/^[\],:{}\s]*$/.test(response.replace(/\\["\\\/bfnrtu]/g, '@').replace(/"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g, ']').replace(/(?:^|:|,)(?:\s*\[)+/g, ''))) {
                            if ($('#container #nReasonList li').text().trim() == 'موردی وجود ندارد.') {
                                $('#container #nReasonList li').remove();
                            }
                            $('#nReasonInput input').val('');
                            var obj = JSON.parse(response);
                            $('#container #nReasonList').append('<div id="' + obj.id + '"><li data-yearNReasonId="' + obj.id + '">' + obj.content + '<span data-yearNReasonId="' + obj.id + '" class="remove" title="حذف" ><img src="<?= BASE_URL ?>assets/img/trash.svg" width="18"></span></li></div>');
                        } else {
                            swal({
                                text: response,
                                icon: "error",
                                className: 'bb',
                                button: {
                                    text: 'باشه',
                                    className: 'sab'
                                },
                            });
                        }
                    }

                });
            }

            // adding new positive reason (ajax)
            // $('#pReasonInput button').click(function(e) {
            //     e.preventDefault();
            //     addNewGoal();
            // });

            $('#pReasonInput input').keydown(function(e) {
                if (e.keyCode === 13) {
                    addNewPReason();
                }
            });

            function addNewPReason() {
                var newPReason = $('#pReasonInput input').val();
                $.ajax({
                    url: "process/ajaxHandler.php",
                    type: 'post',
                    data: {
                        action: "addPReason",
                        newYearPReason: newPReason,
                        yearGoalId: <?= isset($_GET['yearGoalId']) ? ($_GET['yearGoalId']) : null; ?>
                    },
                    success: function(response) {

                        if (/^[\],:{}\s]*$/.test(response.replace(/\\["\\\/bfnrtu]/g, '@').replace(/"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g, ']').replace(/(?:^|:|,)(?:\s*\[)+/g, ''))) {
                            if ($('#container #pReasonList li').text().trim() == 'موردی وجود ندارد.') {
                                $('#container #pReasonList li').remove();
                            }
                            $('#pReasonInput input').val('');
                            var obj = JSON.parse(response);
                            $('#container #pReasonList').append('<div id="' + obj.id + '"><li data-yearPReasonId="' + obj.id + '">' + obj.content + '<span data-yearPReasonId="' + obj.id + '" class="remove" title="حذف" ><img src="<?= BASE_URL ?>assets/img/trash.svg" width="18"></span></li></div>');
                        } else {
                            swal({
                                text: response,
                                icon: "error",
                                className: 'bb',
                                button: {
                                    text: 'باشه',
                                    className: 'sab'
                                },
                            });
                        }
                    }

                });
            }

            // deleting year nReason

            $('body').on('click', '#nReasonList .remove', function(e) {
                var obj = $(this);
                var parentObj = obj.parents("#nReasonList");
                swal({
                        title: 'توجه',
                        text: 'آیا مطمئن هستید؟',
                        icon: 'warning',
                        className: "DBox",
                        buttons: {
                            cancel: {
                                visible: true,
                                text: "لغو",
                                className: 'cancelb'
                            },
                            confirm: {
                                text: 'تایید',
                                className: 'confirmb'
                            }
                        },
                        dangerMode: true,
                    })
                    .then((willDelete) => {
                        if (willDelete) {
                            var nrid = obj.parent().attr('data-yearNReasonId');
                            $.ajax({
                                url: "process/ajaxHandler.php",
                                type: 'post',
                                data: {
                                    action: "deleteYearNReason",
                                    yearNReasonId: nrid
                                },
                                success: function(response) {
                                    obj.parents('#nReasonList').find("[id='" + nrid + "']").fadeOut(1000, function() {
                                        obj.parents('ul').find("[id='" + nrid + "']").remove();
                                    });
                                    noProgramForYearNReason(parentObj);

                                }
                            });
                        }
                    });
            });

            function noProgramForYearNReason(parentObj) {
                if (parentObj.children().length == 1) {
                    parentObj.html('<li style="color: #d05151; list-style-type: none;">موردی وجود ندارد.</li>');
                }
            }

            // deleting year PReason

            $('body').on('click', '#pReasonList .remove', function(e) {
                var obj = $(this);
                var parentObj = obj.parents("#pReasonList");
                swal({
                        title: 'توجه',
                        text: 'آیا مطمئن هستید؟',
                        icon: 'warning',
                        className: "DBox",
                        buttons: {
                            cancel: {
                                visible: true,
                                text: "لغو",
                                className: 'cancelb'
                            },
                            confirm: {
                                text: 'تایید',
                                className: 'confirmb'
                            }
                        },
                        dangerMode: true,
                    })
                    .then((willDelete) => {
                        if (willDelete) {
                            var prid = obj.parent().attr('data-yearPReasonId');
                            $.ajax({
                                url: "process/ajaxHandler.php",
                                type: 'post',
                                data: {
                                    action: "deleteYearPReason",
                                    yearPReasonId: prid
                                },
                                success: function(response) {
                                    obj.parents('#pReasonList').find("[id='" + prid + "']").fadeOut(1000, function() {
                                        obj.parents('ul').find("[id='" + prid + "']").remove();
                                    });
                                    noProgramForYearPReason(parentObj);

                                }
                            });
                        }
                    });
            });

            function noProgramForYearPReason(parentObj) {
                if (parentObj.children().length == 1) {
                    parentObj.html('<li style="color: #d05151; list-style-type: none;">موردی وجود ندارد.</li>');
                }
            }
        });
    </script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
</body>

</html>