<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="<?= BASE_URL ?>assets/img/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="<?= BASE_URL ?>assets/css/style1.css">
    <title><?= SITE_TITLE ?></title>
</head>

<body>
    <div id="header">
        <h1>مدیریت برنامه‌ها و اهداف</h1>
        <ul id="goalsManager">
            <li><a href="<?= BASE_URL ?>primarylist.php" id="primarylist"><img src="<?= BASE_URL ?>assets/img/primarylist.svg" width="16">لیست اولیه</a></li>
            <li><a href="<?= BASE_URL ?>"><img src="<?= BASE_URL ?>assets/img/tasks.svg" width="16">مدیریت برنامه‌ها</a></li>

            <li><a href="<?= BASE_URL . 'setting.php' ?>" title="تنظیمات"><img src="<?= BASE_URL ?>assets/img/setting.svg" width="16">
                    تنظیمات
                </a>
            </li>
            <li><a href="<?= BASE_URL . '?logout=1' ?>" title="خروج"><img src="<?= BASE_URL ?>assets/img/exit.svg" width="16">
                    خروج
                </a>
            </li>
        </ul>

    </div>


    <div id="header2">

        <img id="profileImg" src="<?= $_SESSION['login']->image ?>" alt="" width="35" height="35">
        <?php if ($currentTime > 00 and $currentTime < 12) : ?>
            <p style="display: inline;">صبح بخیر <span><?= $username ?></span> عزیز</p>
        <?php elseif ($currentTime >= 12 and $currentTime < 13) : ?>
            <p style="display: inline;">ظهر بخیر <span><?= $username ?></span> عزیز</p>
        <?php elseif ($currentTime >= 13 and $currentTime < 16) : ?>
            <p style="display: inline;">بعد از ظهر بخیر <span><?= $username ?></span> عزیز</p>
        <?php elseif ($currentTime >= 16 and $currentTime <= 24) : ?>
            <p style="display: inline;">وقت بخیر <span><?= $username ?></span> عزیز</p>
        <?php endif; ?>

        <?php
        $r = rand(0, 37);
        ?>
        <p><?= $beliefs[$r]; ?></p>
        <?php unset($beliefs); ?>
    </div>

    <div id="container">

        <div id="sidebar">
            <div id="categories">
                <h3>دسته‌بندی‌ها</h3>

                <ul id="folders">
                    <a href="<?= BASE_URL . 'goalsManager.php' ?>">
                        <li class="<?= isset($_GET['folderId']) ? '' : 'active' ?> all">
                            <img src="<?= BASE_URL ?>assets/img/cat.svg" width="14"> همه
                            <div class="<?= isset($_GET['folderId']) ? '' : 'arrow' ?>"></div>
                        </li>
                    </a>
                    <?php foreach ($folders as $folder) : ?>
                        <div id="<?= $folder->id ?>">
                            <a href="?folderId=<?= $folder->id; ?>">
                                <li class="<?php
                                            if (isset($_GET['folderId'])) {
                                                echo ($_GET['folderId'] == $folder->id) ? 'active' : '';
                                            }
                                            ?>">
                                    <img src="<?= BASE_URL ?>assets/img/cat.svg" width="14"><?= $folder->name; ?>
                            </a>
                            <span class="remove" title="حذف" data-folderId="<?= $folder->id ?>">
                                <img src="<?= BASE_URL ?>assets/img/trashForCat.svg" width="18">
                            </span>
                            <div class="<?php
                                        if (isset($_GET['folderId'])) {
                                            echo ($_GET['folderId'] == $folder->id) ? 'arrow' : '';
                                        }
                                        ?>">
                            </div>
                            </li>
                        </div>
                    <?php endforeach; ?>

                </ul>
                <br>
                <form action="" method="POST">
                    <div id="addNewCategoryBox">
                        <input type="text" class="addNew" placeholder="افزودن دسته‌بندی جدید" onfocus="$(this).val('')">
                        <button class="addNewBtn">+</button>
                    </div>
                </form>

            </div>
        </div>

        <div id="tasks">
            <div id="today">
                <span class="searchIcon"><img src="<?= BASE_URL ?>assets/img/search_icon.png" width="22"></span>
                <div id="titr">

                    <h2>اهداف</h2>
                    <span id="date">
                        <img src="<?= BASE_URL ?>assets/img/calendar.svg" width="18">امروز <?= $v->format('l j F Y'); ?>
                    </span>
                    <div id="searchForGoals">
                        <p id="searchp">جستجوی اهداف، تصمیمها و یا ارزیابی‌ها:</p>
                        <input type="search" id="psearchg" name="psearch">
                        <label for="whichPart">جستجو در</label>
                        <select id="whichPart">
                            <option value="goals">اهداف ماه‌ها</option>
                            <option value="decisions">تصمیمات مهم</option>
                            <option value="assessments">ارزیابی‌ها</option>
                        </select>
                        <button>
                            برو
                        </button>
                    </div>

                </div>


                <!-- adding tabs -->
                <?php $engMonths = ['farvardin', 'ordibehesht', 'khordad', 'tir', 'mordad', 'shahrivar', 'mehr', 'aban', 'azar', 'dey', 'bahman', 'esfand']; ?>
                <?php $faMonth = ['فروردین', 'اردیبهشت', 'خرداد', 'تیر', 'مرداد', 'شهریور', 'مهر', 'آبان', 'آذر', 'دی', 'بهمن', 'اسفند'] ?>

                <section class="accMenu">
                    <div class="accTitles">
                        <?php for ($i = 0; $i <= 11; $i++) : ?>
                            <div class="accTitle <?= ($currentMonth == $faMonth[$i]) ? 'defaultTab' : '' ?>" data-tab="<?= $engMonths[$i] ?>"><?= $faMonth[$i] ?></div>



                            <div class="accContents">
                                <div id="<?= $engMonths[$i] ?>" class="accContent <?= ($currentMonth == $faMonth[$i]) ? 'default' : '' ?>">
                                    <div class="goals">
                                        <h3 class="subheading">اهداف</h3>
                                        <input type="text" class="addMonthGoal" placeholder="افزودن هدف ماهیانه جدید">
                                        <ul>

                                        </ul>
                                    </div>
                                    <div class="decisions">
                                        <h3 class="subheading">تصمیمات مهم</h3>
                                        <input type="text" class="addDecision" placeholder="افزودن تصمیم جدید">
                                        <ul>

                                        </ul>
                                    </div>
                                    <div class="assessment">
                                        <h3 class="subheading">ارزیابی</h3>
                                        <input type="text" class="addAssessment" placeholder="افزودن ارزیابی جدید">
                                        <ul>

                                        </ul>
                                    </div>
                                    <div class="clear"></div>
                                </div>

                            </div>
                        <?php endfor; ?>
                    </div>



                    <div class="accTitles">
                        <div class="accTitle" data-tab="searchResults" id="ss" style="display:none;">نتایج جستجو</div>
                        <div class="accContents">
                            <div id="searchResults" class="accContent">
                                <ul>

                                </ul>
                                <div id="pagination">

                                </div>
                            </div>
                        </div>
                    </div>
                </section>

            </div>
            <br>



            <!-- End adding tabs -->

            <div id="invisible">مخفی کردن اهداف سالیانه</div><br>
            <input type="text" class="addNew toBeInvisible" placeholder="افزودن هدف سالیانه جدید" onfocus="$(this).val('')">
            <select name="yearSelect" id="yearSelect" class="toBeInvisible">

                <option value="<?= $v->addYear(-1)->format('Y') ?>"><?= $v->format('Y') ?></option>
                <option value="<?= $v->addYear()->format('Y') ?>" selected><?= $v->format('Y') ?></option>
                <?php for ($i = 1; $i <= 10; $i++) : ?>
                    <option value="<?= $v->addYear()->format('Y') ?>"><?= $v->format('Y') ?></option>
                <?php endfor; ?>
                <?php $v->addYear(-10) ?>

            </select>
            <button class="addNewBtn toBeInvisible">+</button>



            <fieldset id="annualGoals" class="toBeInvisible">
                <legend>
                    <span>اهداف سال </span>
                    <select name="yearGoalSelect" id="yearGoalSelect">
                        <option value="<?= $v->addYear(-10)->format('Y') ?>"><?= $v->format('Y') ?></option>
                        <?php for ($i = -9; $i <= 10; $i++) : ?>
                            <option value="<?= $v->addYear()->format('Y') ?>" <?= ($v->format('Y') == $currentYear ? 'selected' : '') ?>><?= $v->format('Y') ?></option>
                        <?php endfor; ?>
                        <?php $v->addYear(-10); ?>
                    </select>
                </legend>
                <ul>
                    <?php if (sizeof($yearGoals) > 0) : ?>
                        <?php foreach ($yearGoals as $yearGoal) : ?>
                            <div id="<?= $yearGoal->id ?>">
                                <li data-yearGoalId="<?= $yearGoal->id ?>" class="clickable <?= $yearGoal->is_done ? 'done' : '' ?>"><?= $yearGoal->is_done ? '<img src="' . BASE_URL . 'assets/img/goal.svg" width="18">' : '<img src="' . BASE_URL . 'assets/img/goal.svg" width="18">' ?><?= $yearGoal->content ?><br>
                                    <span class="remove" title="حذف" data-yearGoalId="<?= $yearGoal->id ?>">
                                        <img src="<?= BASE_URL ?>assets/img/trash.svg" width="18">
                                    </span><a class="baha" title="بهای هدف" href="baha.php?yearGoalId=<?= $yearGoal->id ?>">
                                        <img src="<?= BASE_URL ?>assets/img/baha.svg" width="19">
                                    </a>
                                    <a class="baha" title="دلیل" href="reason.php?yearGoalId=<?= $yearGoal->id ?>">
                                        <img src="<?= BASE_URL ?>assets/img/reason.svg" width="17">
                                    </a>
                                </li>
                            </div>
                        <?php endforeach; ?>
                    <?php else : ?>
                        <li style="color: #d05151;">
                            <img src="<?= BASE_URL ?>assets/img/alarm.svg" width="18">هدفی تعیین نشده است!
                        </li>
                    <?php endif; ?>
                </ul>
            </fieldset>


        </div>
    </div>
    </div>
    <footer>
        <div id="copyright">تمام حقوق این برنامه محفوظ و متعلق به سایت راز کد است.</div>
        <div id="programmer">برنامه ساز: علیرضا پورهادی</div>
    </footer>
    <ul id="goalsManagerM">
        <li><a href="<?= BASE_URL ?>primarylist.php" id="primarylist"><img src="<?= BASE_URL ?>assets/img/primarylist.svg" width="18">لیست اولیه</a></li>
        <li><a href="<?= BASE_URL ?>"><img src="<?= BASE_URL ?>assets/img/tasks.svg" width="16">مدیریت برنامه‌ها</a></li>

        <li><a href="<?= BASE_URL . 'setting.php' ?>" title="تنظیمات"><img src="<?= BASE_URL ?>assets/img/setting.svg" width="16">
                تنظیمات
            </a>
        </li>
        <li><a href="<?= BASE_URL . '?logout=1' ?>" title="خروج"><img src="<?= BASE_URL ?>assets/img/exit.svg" width="16">
                خروج
            </a>
        </li>
    </ul>
    <script src="<?= BASE_URL ?>assets/js/jquery-3.5.1.min.js"></script>

    <script>
        $(document).ready(function() {
            var message = ['تبریک میگم', 'دمت گرم', 'تو بی‌نظیری', 'تو فوق‌العاده‌ای', 'تو بهترینی', 'تو خارق‌العاده‌ای'];

            $('.defaultTab').next().find('.default').slideDown();

            var clickedTab = $(document).find('.defaultTab').text().trim();
            var monthGoalBox = $('#' + $(document).find('.defaultTab').attr('data-tab') + ' .goals ul');
            var decisionBox = $('#' + $(document).find('.defaultTab').attr('data-tab') + ' .decisions ul');
            var assessmentBox = $('#' + $(document).find('.defaultTab').attr('data-tab') + ' .assessment ul');
            getMonthGoals(clickedTab, monthGoalBox);
            getDecisions(clickedTab, decisionBox);
            getAssessments(clickedTab, assessmentBox);

            $('.searchIcon').click(function() {
                if ($(this).html() == '<img src="<?= BASE_URL ?>assets/img/close.png" width="22">') {
                    $(this).html('<img src="<?= BASE_URL ?>assets/img/search_icon.png" width="22">');
                    $('#searchForGoals').animate({
                        opacity: 0
                    });
                } else {
                    $(this).html('<img src="<?= BASE_URL ?>assets/img/close.png" width="22">');
                    $('#searchForGoals').animate({
                        opacity: 1
                    });
                }
            });

            $('#invisible').click(function() {
                if ($(this).text() == 'مخفی کردن اهداف سالیانه') {
                    $(this).text('ظاهر کردن اهداف سالیانه');
                } else {
                    $(this).text('مخفی کردن اهداف سالیانه');
                }
                $('.toBeInvisible').slideToggle();
            });

            $('.accTitle').click(function() {
                $(this).parent().parent().find('.accContent').slideUp();
                if ($(this).next().find('.accContent').css('display') == 'none') {
                    $(this).next().find('.accContent').slideDown();
                }

                var clickedTab = ($(this).text().trim());
                var monthGoalBox = $('#' + $(this).attr('data-tab') + ' .goals ul');
                var decisionsBox = $('#' + $(this).attr('data-tab') + ' .decisions ul');
                var assessmentBox = $('#' + $(this).attr('data-tab') + ' .assessment ul');
                getMonthGoals(clickedTab, monthGoalBox);
                getDecisions(clickedTab, decisionsBox);
                getAssessments(clickedTab, assessmentBox);

            });


            function getMonthGoals(clickedTab, monthGoalBox) {

                $.ajax({
                    url: "process/ajaxHandler.php",
                    type: 'post',
                    data: {
                        action: "getMonthGoals",
                        whichMonth: clickedTab,
                        folderId: <?= isset($_GET['folderId']) ? $_GET["folderId"] : '"all"' ?>
                    },
                    success: function(response) {
                        var obj = JSON.parse(response);
                        monthGoalBox.empty();
                        if (obj.length == 0) {
                            monthGoalBox.append('<li style="color: #d05151;"><img src="<?= BASE_URL ?>assets/img/alarm.svg" width="18">هدفی تعیین نشده است!</li>');
                        } else {
                            $.each(obj, function(ind, val) {
                                if (val.is_done == 1) {
                                    var myClassName = 'clickable done';
                                } else if (val.is_done == 0) {
                                    var myClassName = 'clickable';
                                }
                                monthGoalBox.append('<div id="' + val.id + '"><li data-monthGoalId="' + val.id + '" class="' + myClassName + '"><img src="<?= BASE_URL ?>assets/img/goal.svg" width="18">' + val.content + '<span class="remove" title="حذف" data-monthGoalId="' + val.id + '"><img src="<?= BASE_URL ?>assets/img/trash.svg" width="18"></span></li></div>');
                            })
                        }
                    }
                });
            }

            function getDecisions(clickedTab, decisionsBox) {
                $.ajax({
                    url: "process/ajaxHandler.php",
                    type: 'post',
                    data: {
                        action: "getDecisions",
                        whichMonth: clickedTab,
                        folderId: <?= isset($_GET['folderId']) ? $_GET["folderId"] : '"all"' ?>
                    },
                    success: function(response) {
                        var obj = JSON.parse(response);
                        decisionsBox.empty();
                        if (obj.length == 0) {
                            decisionsBox.append('<li style="color: #d05151;"><img src="<?= BASE_URL ?>assets/img/alarm.svg" width="18">تصمیمی اتخاد نشده است!</li>');
                        } else {
                            $.each(obj, function(ind, val) {
                                if (val.is_done == 1) {
                                    var myClassName = 'clickable done';
                                } else if (val.is_done == 0) {
                                    var myClassName = 'clickable';
                                }
                                decisionsBox.append('<div id="' + val.id + '"><li data-decisionId="' + val.id + '" class="' + myClassName + '"><img src="<?= BASE_URL ?>assets/img/decision.svg" width="18">' + val.content + '<span class="remove" title="حذف" data-decisionId="' + val.id + '"><img src="<?= BASE_URL ?>assets/img/trash.svg" width="18"></span></li></div>');
                            })
                        }
                    }
                });
            }

            function getAssessments(clickedTab, assessmentBox) {
                $.ajax({
                    url: "process/ajaxHandler.php",
                    type: 'post',
                    data: {
                        action: "getAssessments",
                        whichMonth: clickedTab,
                        folderId: <?= isset($_GET['folderId']) ? $_GET["folderId"] : '"all"' ?>
                    },
                    success: function(response) {
                        var obj = JSON.parse(response);
                        assessmentBox.empty();
                        if (obj.length == 0) {
                            assessmentBox.append('<li style="color: #d05151;"><img src="<?= BASE_URL ?>assets/img/alarm.svg" width="18">ارزیابی انجام نشده است!</li>');
                        } else {
                            $.each(obj, function(ind, val) {
                                if (val.is_done == 1) {
                                    var myClassName = 'clickable done';
                                } else if (val.is_done == 0) {
                                    var myClassName = 'clickable';
                                }
                                assessmentBox.append('<div id="' + val.id + '"><li data-assessmentId="' + val.id + '" class="' + myClassName + '"><img src="<?= BASE_URL ?>assets/img/assessment.svg" width="18">' + val.content + '<span class="remove" title="حذف" data-assessmentId="' + val.id + '"><img src="<?= BASE_URL ?>assets/img/trash.svg" width="18"></span></li></div>');
                            })
                        }
                    }
                });
            }

            $('#sidebar .addNewBtn').click(function(e) {
                e.preventDefault();
                var folderNameInput = $('#sidebar .addNew');
                $.ajax({
                    url: "process/ajaxHandler.php",
                    type: 'post',
                    data: {
                        action: "addFolder",
                        folderName: folderNameInput.val()
                    },
                    success: function(response) {
                        if (/^[\],:{}\s]*$/.test(response.replace(/\\["\\\/bfnrtu]/g, '@').replace(/"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g, ']').replace(/(?:^|:|,)(?:\s*\[)+/g, ''))) {
                            folderNameInput.val('');
                            var obj = JSON.parse(response);
                            $("#folders").append('<div id="' + obj.id + '"><a href="?folderId=' + obj.id + '"><li><img src="<?= BASE_URL ?>assets/img/cat.svg" width="14">' + obj.name + '</a><span class="remove" title="حذف" data-folderId="' + obj.id + '"><img src="<?= BASE_URL ?>assets/img/trash.svg" width="18"></span></li></div>');
                        } else {
                            swal({
                                title: 'توجه',
                                text: response,
                                icon: "error",
                                className: 'bb',
                                button: {
                                    text: 'باشه',
                                    className: 'sab'
                                },
                            });
                        }
                    }
                });
            });

            $('div').on('click', '#folders .remove', function(e) {
                e.stopPropagation();
                var folderName = $(this).parent().text();
                folderName = folderName.trim();

                swal({
                        title: 'توجه',
                        text: 'آیا مطمئن هستید؟',
                        icon: 'warning',
                        className: "DBox",
                        buttons: {
                            cancel: {
                                visible: true,
                                text: "لغو",
                                className: 'cancelb'
                            },
                            confirm: {
                                text: 'تایید',
                                className: 'confirmb'
                            }
                        },
                        dangerMode: true,
                    })
                    .then((willDelete) => {
                        if (willDelete) {
                            var fid = $(this).attr('data-folderId');
                            $.ajax({
                                url: "process/ajaxHandler.php",
                                type: 'post',
                                data: {
                                    action: "deleteFolder",
                                    folderId: fid
                                },
                                success: function(response) {
                                    $('#folders .remove').parents('#folders').children("div[id|='" + fid + "']").fadeOut(700);
                                }
                            });
                        }
                    });
            });

            $('div').on('click', '#annualGoals .remove', function(e) {
                e.stopPropagation();
                var obj = $(this);
                var parentObjId = obj.parents("[id^='annualGoals']").attr('id');
                var goalContent = obj.parent().text();
                goalContent = goalContent.trim();
                swal({
                        title: 'توجه',
                        text: 'آیا مطمئن هستید؟',
                        icon: 'warning',
                        className: "DBox",
                        buttons: {
                            cancel: {
                                visible: true,
                                text: "لغو",
                                className: 'cancelb'
                            },
                            confirm: {
                                text: 'تایید',
                                className: 'confirmb'
                            }
                        },
                        dangerMode: true,
                    })
                    .then((willDelete) => {
                        if (willDelete) {
                            var ygid = obj.parent().attr('data-yearGoalId');
                            $.ajax({
                                url: "process/ajaxHandler.php",
                                type: 'post',
                                data: {
                                    action: "deleteYearGoal",
                                    yearGoalId: ygid
                                },
                                success: function(response) {
                                    obj.parents('#annualGoals').find("div[id|='" + ygid + "']").remove();
                                    noProgram(parentObjId);
                                }
                            });
                        }
                    });
            });

            function noProgram(id) {
                if ($('#' + id + " ul").children().length == 0) {
                    $('#' + id + " ul").html('<li style="color: #d05151;"><img src="<?= BASE_URL ?>assets/img/alarm.svg" width="18">هدفی تعیین نشده است!</li>');
                }
            }

            $('div').on('click', 'fieldset#annualGoals li', function(e) {
                e.stopPropagation();
                var obj = $(this);
                var gid = $(this).attr('data-yearGoalId');
                $.ajax({
                    url: "process/ajaxHandler.php",
                    type: 'post',
                    data: {
                        action: "yearGoalDoneSwitch",
                        yearGoalId: gid
                    },
                    success: function(response) {
                        if (response == 1) {
                            obj.removeClass();
                            obj.addClass('clickable done');
                            obj.find('img')[0].remove();
                            obj.prepend('<img src="<?= BASE_URL ?>assets/img/goal.svg" width="18">');
                            var j = rand(0, 6);
                            swal({
                                title: message[j],
                                text: 'تو موفق شدی که به این هدف خود برسی!',
                                icon: "success",
                                className: 'bb',
                                button: {
                                    text: 'سپاس',
                                    className: 'sab'
                                },
                            });
                        } else if (response == 0) {
                            obj.removeClass();
                            obj.addClass('clickable');
                            obj.find('img')[0].remove();
                            obj.prepend('<img src="<?= BASE_URL ?>assets/img/goal.svg" width="18">');
                        }
                    }
                });
            });

            $('#tasks .addNewBtn').click(function(e) {
                e.preventDefault();
                addNewGoal();
            });

            $('#tasks .addNew').keydown(function(e) {
                if (e.keyCode === 13) {
                    addNewGoal();
                }
            });

            function addNewGoal() {
                var taskTitleInput = $('#tasks input.addNew');
                var dateSelect = $('#tasks select#yearSelect').val();
                var goalBox = $('fieldset#annualGoals ul');
                var currentYear = <?= $v->format('Y') ?>;
                $.ajax({
                    url: "process/ajaxHandler.php",
                    type: 'post',
                    data: {
                        action: "addGoal",
                        dateSelect: dateSelect,
                        taskTitle: taskTitleInput.val(),
                    },
                    success: function(response) {
                        if (goalBox.text().trim() == 'هدفی تعیین نشده است!') {
                            goalBox.empty();
                        }
                        if (/^[\],:{}\s]*$/.test(response.replace(/\\["\\\/bfnrtu]/g, '@').replace(/"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g, ']').replace(/(?:^|:|,)(?:\s*\[)+/g, ''))) {
                            taskTitleInput.val('');
                            if (dateSelect == currentYear) {
                                var obj = JSON.parse(response);
                                goalBox.append('<div id="' + obj.id + '"><li data-yearGoalId="' + obj.id + '" class="clickable"><img src="<?= BASE_URL ?>assets/img/goal.svg" width="18">' + obj.content + '<br><span data-yearGoalId="' + obj.id + '" class="remove" title="حذف" ><img src="<?= BASE_URL ?>assets/img/trash.svg" width="18"></span><a class="baha" title="بهای هدف" href="baha.php?yearGoalId=' + obj.id + '"><img src="<?= BASE_URL ?>assets/img/baha.svg" width="19"></a><a class="baha" title="دلیل" href="reason.php?yearGoalId=' + obj.id + '"><img src="<?= BASE_URL ?>assets/img/reason.svg" width="17"></a></li></div>');
                            } else {
                                swal({
                                    text: 'هدف مورد نظر به سال ' + dateSelect + ' افزوده شد.',
                                    icon: "success",
                                    className: 'bb',
                                    button: {
                                        text: 'باشه',
                                        className: 'sab'
                                    },
                                });
                            }

                        } else {
                            swal({
                                title: 'توجه',
                                text: response,
                                icon: "error",
                                className: 'bb',
                                button: {
                                    text: 'باشه',
                                    className: 'sab'
                                },
                            });
                        }
                    }
                });
            }

            $('#yearGoalSelect').change(function() {
                var yearGoalSelectValue = $(this).val();
                var goalBox = $('fieldset#annualGoals ul');
                $.ajax({
                    url: "process/ajaxHandler.php",
                    type: 'post',
                    data: {
                        action: "showYearGoals",
                        whichYear: yearGoalSelectValue
                    },
                    success: function(response) {
                        var obj = JSON.parse(response);
                        goalBox.empty();
                        if (obj.length == 0) {
                            goalBox.append('<li style="color: #d05151;"><img src="<?= BASE_URL ?>assets/img/alarm.svg" width="18">هدفی تعیین نشده است!</li>');
                        } else {
                            $.each(obj, function(ind, val) {
                                if (val.is_done == 1) {
                                    var myClassName = 'clickable done';
                                } else if (val.is_done == 0) {
                                    var myClassName = 'clickable';
                                }
                                goalBox.append('<div id="' + val.id + '"><li data-yearGoalId="' + val.id + '" class="' + myClassName + '"><img src="<?= BASE_URL ?>assets/img/goal.svg" width="18">' + val.content + '<br><span class="remove" title="حذف" data-yearGoalId="' + val.id + '"><img src="<?= BASE_URL ?>assets/img/trash.svg" width="18"></span><a class="baha" title="بهای هدف" href="baha.php?yearGoalId=' + val.id + '"><img src="<?= BASE_URL ?>assets/img/baha.svg" width="19"></a><a class="baha" title="دلیل" href="reason.php?yearGoalId=' + val.id + '"><img src="<?= BASE_URL ?>assets/img/reason.svg" width="17"></a></li></div>');
                            })
                        }
                    }
                })
            });

            $('.addMonthGoal').keydown(function(e) {
                if (e.keyCode === 13) {
                    var addNewMonthGoalVal = $(this).val();
                    var goalsId = $(this).parents('.goals').parent().attr('id');
                    var goalDiv = $(this).parents('.goals');
                    var whichMonth = $(document).find("[data-tab='" + goalsId + "']").text().trim();
                    addNewMonthGoal(addNewMonthGoalVal, whichMonth, goalDiv);
                    $(this).val('');
                }
            });

            function addNewMonthGoal(val, whichMonth, goalDiv) {
                goalDiv = goalDiv.find('ul');
                $.ajax({
                    url: "process/ajaxHandler.php",
                    type: 'post',
                    data: {
                        action: "addNewMonthGoal",
                        whichMonth: whichMonth,
                        newMonthGoal: val,
                        folderId: <?= isset($_GET['folderId']) ? $_GET["folderId"] : '"all"' ?>,
                    },
                    success: function(response) {
                        if (/^[\],:{}\s]*$/.test(response.replace(/\\["\\\/bfnrtu]/g, '@').replace(/"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g, ']').replace(/(?:^|:|,)(?:\s*\[)+/g, ''))) {
                            if (goalDiv.text().trim() == 'هدفی تعیین نشده است!') {
                                goalDiv.empty();
                            }
                            var obj = JSON.parse(response);
                            if (obj.is_done == 1) {
                                var myClassName = 'clickable done';
                            } else if (obj.is_done == 0) {
                                var myClassName = 'clickable';
                            }
                            goalDiv.append('<div id="' + obj.id + '"><li data-monthGoalId="' + obj.id + '" class="' + myClassName + '"><img src="<?= BASE_URL ?>assets/img/goal.svg" width="18">' + obj.content + '<span class="remove" title="حذف" data-monthGoalId="' + obj.id + '"><img src="<?= BASE_URL ?>assets/img/trash.svg" width="18"></li></div>');
                        } else {
                            swal({
                                title: 'توجه',
                                text: response,
                                icon: "error",
                                className: 'bb',
                                button: {
                                    text: 'باشه',
                                    className: 'sab'
                                },
                            });
                        }
                    }

                });
            }

            $('.addDecision').keydown(function(e) {
                if (e.keyCode === 13) {
                    var addNewDecisionVal = $(this).val();
                    var decisionId = $(this).parents('.decisions').parent().attr('id');
                    var decisionDiv = $(this).parents('.decisions');
                    var whichMonth = $(document).find("[data-tab='" + decisionId + "']").text().trim();
                    addNewDecision(addNewDecisionVal, whichMonth, decisionDiv);
                    $(this).val('');
                }
            });

            function addNewDecision(val, whichMonth, decisionDiv) {
                decisionDiv = decisionDiv.find('ul');
                $.ajax({
                    url: "process/ajaxHandler.php",
                    type: 'post',
                    data: {
                        action: "addNewDecision",
                        whichMonth: whichMonth,
                        newDecision: val,
                        folderId: <?= isset($_GET['folderId']) ? $_GET["folderId"] : '"all"' ?>,
                    },
                    success: function(response) {
                        if (/^[\],:{}\s]*$/.test(response.replace(/\\["\\\/bfnrtu]/g, '@').replace(/"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g, ']').replace(/(?:^|:|,)(?:\s*\[)+/g, ''))) {
                            if (decisionDiv.text().trim() == 'تصمیمی اتخاد نشده است!') {
                                decisionDiv.empty();
                            }
                            var obj = JSON.parse(response);
                            if (obj.is_done == 1) {
                                var myClassName = 'clickable done';
                            } else if (obj.is_done == 0) {
                                var myClassName = 'clickable';
                            }
                            decisionDiv.append('<div id="' + obj.id + '"><li data-decisionId="' + obj.id + '" class="' + myClassName + '"><img src="<?= BASE_URL ?>assets/img/decision.svg" width="18">' + obj.content + '<span class="remove" title="حذف" data-decisionId="' + obj.id + '"><img src="<?= BASE_URL ?>assets/img/trash.svg" width="18"></span></li></div>');
                        } else {
                            swal({
                                title: 'توجه',
                                text: response,
                                icon: "error",
                                className: 'bb',
                                button: {
                                    text: 'باشه',
                                    className: 'sab'
                                },
                            });
                        }
                    }

                });
            }

            $('.addAssessment').keydown(function(e) {
                if (e.keyCode === 13) {
                    var addNewAssessmentVal = $(this).val();
                    var assessmentId = $(this).parents('.assessment').parent().attr('id');
                    var assessmentDiv = $(this).parents('.assessment');
                    var whichMonth = $(document).find("[data-tab='" + assessmentId + "']").text().trim();
                    addNewAssessment(addNewAssessmentVal, whichMonth, assessmentDiv);
                    $(this).val('');
                }
            });

            function addNewAssessment(val, whichMonth, assessmentDiv) {
                assessmentDiv = assessmentDiv.find('ul');
                $.ajax({
                    url: "process/ajaxHandler.php",
                    type: 'post',
                    data: {
                        action: "addNewAssessment",
                        whichMonth: whichMonth,
                        newAssessment: val,
                        folderId: <?= isset($_GET['folderId']) ? $_GET["folderId"] : '"all"' ?>,
                    },
                    success: function(response) {
                        if (/^[\],:{}\s]*$/.test(response.replace(/\\["\\\/bfnrtu]/g, '@').replace(/"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g, ']').replace(/(?:^|:|,)(?:\s*\[)+/g, ''))) {
                            if (assessmentDiv.text().trim() == 'ارزیابی انجام نشده است!') {
                                assessmentDiv.empty();
                            }
                            var obj = JSON.parse(response);

                            assessmentDiv.append('<div id="' + obj.id + '"><li data-assessmentId="' + obj.id + '"><img src="<?= BASE_URL ?>assets/img/assessment.svg" width="18">' + obj.content + '<span class="remove" title="حذف" data-assessmentId="' + obj.id + '"><img src="<?= BASE_URL ?>assets/img/trash.svg" width="18"></span></li></div>');
                        } else {
                            swal({
                                title: 'توجه',
                                text: response,
                                icon: "error",
                                className: 'bb',
                                button: {
                                    text: 'باشه',
                                    className: 'sab'
                                },
                            });
                        }
                    }

                });
            }

            $('div').on('click', '.goals .remove', function(e) {
                e.stopPropagation();
                var obj = $(this);
                var parentObj = obj.parents(".goals ul");
                var goalContent = obj.parent().text();
                goalContent = goalContent.trim();
                swal({
                        title: 'توجه',
                        text: 'آیا مطمئن هستید؟',
                        icon: 'warning',
                        className: "DBox",
                        buttons: {
                            cancel: {
                                visible: true,
                                text: "لغو",
                                className: 'cancelb'
                            },
                            confirm: {
                                text: 'تایید',
                                className: 'confirmb'
                            }
                        },
                        dangerMode: true,
                    })
                    .then((willDelete) => {
                        if (willDelete) {
                            var mgid = obj.parent().attr('data-monthGoalId');
                            $.ajax({
                                url: "process/ajaxHandler.php",
                                type: 'post',
                                data: {
                                    action: "deleteMonthGoal",
                                    monthGoalId: mgid
                                },
                                success: function(response) {
                                    obj.parents('.goals').find("[id='" + mgid + "']").remove();
                                    noProgramForMonthGoal(parentObj);
                                }
                            });
                        }
                    });
            });

            function noProgramForMonthGoal(parentObj) {
                if (parentObj.children().length == 0) {
                    parentObj.html('<li style="color: #d05151;"><img src="<?= BASE_URL ?>assets/img/alarm.svg" width="18">هدفی تعیین نشده است!</li>');
                }
            }

            $('div').on('click', '.decisions .remove', function(e) {
                e.stopPropagation();
                var obj = $(this);
                var parentObj = obj.parents(".decisions ul");
                var decisionsContent = obj.parent().text();
                decisionsContent = decisionsContent.trim();
                swal({
                        title: 'توجه',
                        text: 'آیا مطمئن هستید؟',
                        icon: 'warning',
                        className: "DBox",
                        buttons: {
                            cancel: {
                                visible: true,
                                text: "لغو",
                                className: 'cancelb'
                            },
                            confirm: {
                                text: 'تایید',
                                className: 'confirmb'
                            }
                        },
                        dangerMode: true,
                    })
                    .then((willDelete) => {
                        if (willDelete) {
                            var did = obj.parent().attr('data-decisionId');
                            $.ajax({
                                url: "process/ajaxHandler.php",
                                type: 'post',
                                data: {
                                    action: "deleteDecision",
                                    decisionId: did
                                },
                                success: function(response) {
                                    obj.parents('.decisions').find("[id='" + did + "']").remove();
                                    noProgramForDecision(parentObj);
                                }
                            });
                        }
                    });
            });

            function noProgramForDecision(parentObj) {
                if (parentObj.children().length == 0) {
                    parentObj.html('<li style="color: #d05151;"><img src="<?= BASE_URL ?>assets/img/alarm.svg" width="18">تصمیمی اتخاد نشده است!</li>');
                }
            }

            $('div').on('click', '.assessment .remove', function(e) {
                e.stopPropagation();
                var obj = $(this);
                var parentObj = obj.parents(".assessment ul");
                var assessmentContent = obj.parent().text();
                assessmentContent = assessmentContent.trim();
                swal({
                        title: 'توجه',
                        text: 'آیا مطمئن هستید؟',
                        icon: 'warning',
                        className: "DBox",
                        buttons: {
                            cancel: {
                                visible: true,
                                text: "لغو",
                                className: 'cancelb'
                            },
                            confirm: {
                                text: 'تایید',
                                className: 'confirmb'
                            }
                        },
                        dangerMode: true,
                    })
                    .then((willDelete) => {
                        if (willDelete) {
                            var aid = obj.parent().attr('data-assessmentId');
                            $.ajax({
                                url: "process/ajaxHandler.php",
                                type: 'post',
                                data: {
                                    action: "deleteAssessment",
                                    assessmentId: aid
                                },
                                success: function(response) {
                                    obj.parents('.assessment').find("[id='" + aid + "']").remove();
                                    noProgramForAssessment(parentObj);
                                }
                            });
                        }
                    });
            });

            function noProgramForAssessment(parentObj) {
                if (parentObj.children().length == 0) {
                    parentObj.html('<li style="color: #d05151;"><img src="<?= BASE_URL ?>assets/img/alarm.svg" width="18">ارزیابی انجام نشده است!</li>');
                }
            }

            $('div').on('click', '.goals ul li', function(e) {
                e.stopPropagation();
                var obj = $(this);
                var gid = $(this).attr('data-monthGoalId');
                $.ajax({
                    url: "process/ajaxHandler.php",
                    type: 'post',
                    data: {
                        action: "monthGoalDoneSwitch",
                        monthGoalId: gid
                    },
                    success: function(response) {
                        if (response == 1) {
                            obj.removeClass();
                            obj.addClass('clickable done');
                            obj.find('img')[0].remove();
                            obj.prepend('<img src="<?= BASE_URL ?>assets/img/goal.svg" width="18">');
                            var j = rand(0, 6);
                            swal({
                                title: message[j],
                                text: 'تو موفق شدی که به این هدف خود برسی!',
                                icon: "success",
                                className: 'bb',
                                button: {
                                    text: 'سپاس',
                                    className: 'sab'
                                },
                            });
                        } else if (response == 0) {
                            obj.removeClass();
                            obj.addClass('clickable');
                            obj.find('img')[0].remove();
                            obj.prepend('<img src="<?= BASE_URL ?>assets/img/goal.svg" width="18">')
                        }
                    }
                });
            });

            $('div').on('click', '.decisions ul li', function(e) {
                e.stopPropagation();
                var obj = $(this);
                var did = $(this).attr('data-decisionId');
                $.ajax({
                    url: "process/ajaxHandler.php",
                    type: 'post',
                    data: {
                        action: "decisionDoneSwitch",
                        decisionId: did
                    },
                    success: function(response) {
                        if (response == 1) {
                            obj.removeClass();
                            obj.addClass('clickable done');
                            obj.find('img')[0].remove();
                            obj.prepend('<img src="<?= BASE_URL ?>assets/img/decision.svg" width="18">')
                        } else if (response == 0) {
                            obj.removeClass();
                            obj.addClass('clickable');
                            obj.find('img')[0].remove();
                            obj.prepend('<img src="<?= BASE_URL ?>assets/img/decision.svg" width="18">')
                        }
                    }
                });
            });

            // start search

            $('#searchForGoals button').click(function(e) {
                doSearch(1);
            });

            $('#searchForGoals input').keydown(function(e) {
                if (e.keyCode === 13) {
                    doSearch(1);
                }
            });

            function doSearch(page) {
                var searchEntry = $('#psearchg').val();
                var whichPart = $('#whichPart').val();
                if (whichPart == 'goals') {
                    var action = "doSearchForGoals";
                } else if (whichPart == 'decisions') {
                    var action = "doSearchForDecisions";
                } else if (whichPart == 'assessments') {
                    var action = 'doSearchForAssessments'
                }
                $.ajax({
                    url: "process/ajaxHandler.php",
                    type: 'post',
                    data: {
                        action: action,
                        page: page,
                        searchEntry: searchEntry
                    },
                    success: function(response) {
                        $('#ss').css('display', 'inline-block');
                        $('.default').css('display', 'none');
                        $('.accContent').hide();
                        $('.accTitle').css({
                            backgroundColor: '#ddd'
                        });
                        if ($('#searchResults').css('display') === 'none') {
                            $('#searchResults').fadeIn();
                            $('#ss').css({
                                backgroundColor: '#fafafa',
                                borderBottom: '1px solid #fafafa'
                            });
                        }
                        if (/^[\],:{}\s]*$/.test(response.replace(/\\["\\\/bfnrtu]/g, '@').replace(/"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g, ']').replace(/(?:^|:|,)(?:\s*\[)+/g, ''))) {
                            var searchResultsBox = $('#searchResults ul');
                            var obj = JSON.parse(response);
                            $('#searchResults').find('ul').empty('li, div');

                            $.each(obj, function(ind, val) {
                                if (ind != 'numberOfPages') {

                                    searchResultsBox.append('<div id="' + val.id + '"><li data-taskId="' + val.id + '">' + val.content + '<span class="searchDate">' + val.which_month + '</span></li></div>');
                                } else {
                                    if (val == 0) {
                                        searchResultsBox.append('<li style="color: #d05151;">موردی یافت نشد.</li>');
                                    }
                                    var pageNumber = getPageNumber(val);
                                    var baseUrl = "<?= BASE_URL ?>";

                                    $('#searchResults #pagination').empty();
                                    $('#searchResults #pagination').append("<br><br><br><br><span data-page='" + pageNumber + "' title='صفحه آخر'>«</span>");

                                    for (let i = 1; i <= pageNumber; i++) {

                                        if (i == page) {
                                            $('#searchResults #pagination').append('<strong>' + i + '</strong>');
                                        } else {
                                            $('#searchResults #pagination').append('<span data-page="' + i + '">' + i + '</span>');
                                        }

                                    }
                                    $('#searchResults #pagination').append("<span data-page='1' title='صفحه نخست'>»</span><br><br><br>");
                                    if (pageNumber <= 1) {
                                        $('#searchResults #pagination').empty();
                                    }
                                }
                            });



                        } else {
                            swal({
                                title: 'توجه',
                                text: response,
                                icon: "error",
                                className: 'bb',
                                button: {
                                    text: 'باشه',
                                    className: 'sab'
                                },
                            });
                        }
                    }
                });




            }

            function getPageNumber(val) {
                var tasksPerPage = <?= TASKS_PER_PAGE ?>;
                var pageNumbers = Math.ceil(Number(val) / Number(tasksPerPage));
                return pageNumbers;
            }

            $('div').on('click', '#searchResults #pagination span', function(e) {
                e.stopPropagation();
                doSearch($(this).attr('data-page'));
            });

            // end search

            $('div').on('click', '#annualGoals .baha', function(e) {
                e.stopPropagation();
            });

            function rand(start, end) {
                var r = start + Math.floor(Math.random() * (end - start));
                return r;
            }

        })
    </script>

    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
</body>

</html>