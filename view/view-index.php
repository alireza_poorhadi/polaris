<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="<?= BASE_URL ?>assets/img/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="<?= BASE_URL ?>assets/css/style1.css">
    <title><?= SITE_TITLE ?></title> 
</head>

<body>
    <div id="header">
        <h1>مدیریت برنامه‌ها و اهداف</h1>
        <ul id="goalsManager">
            <li><a href="<?= BASE_URL ?>primarylist.php" id="primarylist"><img src="<?= BASE_URL ?>assets/img/primarylist.svg" width="16">لیست اولیه</a></li>
            <li><a href="goalsManager.php"><img src="<?= BASE_URL ?>assets/img/goal.svg" width="16">مدیریت اهداف</a></li>

            <li><a href="<?= BASE_URL . 'setting.php' ?>" title="تنظیمات"><img src="<?= BASE_URL ?>assets/img/setting.svg" width="16">
                    تنظیمات
                </a>
            </li>
            <li><a href="<?= BASE_URL . '?logout=1' ?>" title="خروج"><img src="<?= BASE_URL ?>assets/img/exit.svg" width="16">
                    خروج
                </a>
            </li>
        </ul>

    </div>


    <div id="header2">

        <img id="profileImg" src="<?= $_SESSION['login']->image ?>" alt="" width="35" height="35">
        <?php if ($currentTime > 00 and $currentTime < 12) : ?>
            <p style="display: inline;">صبح بخیر <span><?= $username ?></span> عزیز</p>
        <?php elseif ($currentTime >= 12 and $currentTime < 13) : ?>
            <p style="display: inline;">ظهر بخیر <span><?= $username ?></span> عزیز</p>
        <?php elseif ($currentTime >= 13 and $currentTime < 16) : ?>
            <p style="display: inline;">بعد از ظهر بخیر <span><?= $username ?></span> عزیز</p>
        <?php elseif ($currentTime >= 16 and $currentTime <= 24) : ?>
            <p style="display: inline;">وقت بخیر <span><?= $username ?></span> عزیز</p>
        <?php endif; ?>
        <?php
        $r = rand(0,37);
        ?>
        <p><?= $beliefs[$r]; ?></p>
        <?php unset($beliefs);?>
    </div>




    <div id="container">

        <div id="sidebar">
            <div id="categories">
                <h3>دسته‌بندی‌ها</h3>

                <ul id="folders">
                    <a href="<?= BASE_URL ?>">
                        <li class="<?= isset($_GET['folderId']) ? '' : 'active' ?> all">
                            <img src="<?= BASE_URL ?>assets/img/cat.svg" width="14"> همه
                        </li>
                    </a>
                    <?php foreach ($folders as $folder) : ?>
                        <div id="<?= $folder->id ?>">
                            <a href="?folderId=<?= $folder->id; ?>">
                                <li class="<?php
                                            if (isset($_GET['folderId'])) {
                                                echo ($_GET['folderId'] == $folder->id) ? 'active' : '';
                                            }
                                            ?>">
                                    <img src="<?= BASE_URL ?>assets/img/cat.svg" width="14"><?= $folder->name; ?>
                            </a>
                            <span class="remove" title="حذف" data-folderId="<?= $folder->id ?>">
                                <img src="<?= BASE_URL ?>assets/img/trashForCat.svg" width="18">
                            </span>

                            </li>
                        </div>
                    <?php endforeach; ?>

                </ul>
                <br>
                <form action="" method="POST">
                    <div id="addNewCategoryBox">
                        <input type="text" class="addNew" placeholder="افزودن دسته‌بندی جدید" onfocus="$(this).val('')">
                        <button class="addNewBtn">+</button>
                    </div>
                </form>

            </div>
        </div>

        <div id="tasks">
            <div id="today">
                <span class="searchIcon"><img src="<?= BASE_URL ?>assets/img/search_icon.png" width="22"></span>
                <div id="titr">

                    <h2>برنامه‌ها</h2>
                    <span id="date" style="display: block;">
                        <img src="<?= BASE_URL ?>assets/img/calendar.svg" width="18">امروز <?= $v->format('l j F Y'); ?>
                    </span>
                    <div id="search">
                        <form action="">
                            <input type="search" id="psearch" name="psearch" placeholder="جستجوی برنامه‌ها">
                            <button>
                                برو
                            </button>
                        </form>
                    </div>

                </div>

                <br><br>
                <fieldset id="daySelect">
                    <legend>افزودن برنامه جدید</legend>
                    <input type="text" class="addNew" onfocus="$(this).val('')" id="addNewTask">
                    <button class="addNewBtn">برو</button>
                    <br>
                    <input type="checkbox" id="emrooz" name="emrooz" value="emrooz" class="days"><label for="emrooz" class="daysLabel">امروز</label>
                    <input type="checkbox" id="1day" name="1day" value="1day" class="days"><label for="1day" class="daysLabel">فردا</label>
                    <input type="checkbox" id="2days" name="2days" value="2days" class="days"><label for="2days" class="daysLabel"><?= $v->addDay(2)->format('l j F') ?></label>
                    <input type="checkbox" id="3days" name="3days" value="3days" class="days"><label for="3days" class="daysLabel"><?= $v->addDay()->format('l j F') ?></label>
                    <input type="checkbox" id="4days" name="4days" value="4days" class="days"><label for="4days" class="daysLabel"><?= $v->addDay()->format('l j F') ?></label>
                    <input type="checkbox" id="5days" name="5days" value="5days" class="days"><label for="5days" class="daysLabel"><?= $v->addDay()->format('l j F') ?></label>
                    <input type="checkbox" id="6days" name="6days" value="6days" class="days"><label for="6days" class="daysLabel"><?= $v->addDay()->format('l j F') ?></label>
                    <br><br>
                </fieldset>
                <br>



                <!-- adding tabs -->
                <section class="accMenu">
                    <div class="accTitles">
                        <div class="accTitles">
                            <div class="accTitle" data-tab="searchResults" id="ss" style="display:none;">نتایج جستجو</div>
                            <div class="accContents">
                                <div id="searchResults" class="accContent">
                                    <ul id="taskEntrysearchResults">
                                    </ul>
                                    <div id=pagination>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="accTitle" data-tab="yesterday">دیروز</div>
                        <div class="accContents">
                            <div id="yesterday" class="accContent">
                                <ul id="taskEntryYesterday">
                                    <?php if (sizeof($yesterdayTasks) > 0) : ?>
                                        <?php foreach ($yesterdayTasks as $yesterdayTask) : ?>
                                            <div id="<?= $yesterdayTask->id ?>">
                                                <li data-taskId="<?= $yesterdayTask->id ?>" class="clickable <?= $yesterdayTask->is_done ? 'done' : '' ?>"><?= $yesterdayTask->is_done ? '<img src="' . BASE_URL . 'assets/img/checkbox.svg" width="20">' : '<img src="' . BASE_URL . 'assets/img/clock.svg" width="20">' ?><?= $yesterdayTask->title ?>
                                                    <span class="remove" title="حذف" data-taskId="<?= $yesterdayTask->id ?>">
                                                        <img src="<?= BASE_URL ?>assets/img/trash.svg" width="18">
                                                    </span></li>
                                            </div>
                                        <?php endforeach; ?>
                                    <?php else : ?>
                                        <li style="color: #d05151;">
                                            <img src="<?= BASE_URL ?>assets/img/alarm.svg" width="18">هنوز برنامه‌ای وجود ندارد.
                                        </li>
                                    <?php endif; ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="accTitles">
                        <div class="accTitle defaultTab" data-tab="todayTab">امروز</div>
                        <div class="accContents">
                            <div id="todayTab" class="accContent default">
                                <ul id="taskEntryToday">
                                    <?php if (sizeof($tasks) > 0) : ?>
                                        <?php foreach ($tasks as $task) : ?>
                                            <div id="<?= $task->id ?>">
                                                <li data-taskId="<?= $task->id ?>" class="clickable <?= $task->is_done ? 'done' : '' ?>"><?= $task->is_done ? '<img src="' . BASE_URL . 'assets/img/checkbox.svg" width="20">' : '<img src="' . BASE_URL . 'assets/img/clock.svg" width="20">' ?><?= $task->title ?>
                                                    <span class="remove" title="حذف" data-taskId="<?= $task->id ?>">
                                                        <img src="<?= BASE_URL ?>assets/img/trash.svg" width="18">
                                                    </span></li>
                                            </div>
                                        <?php endforeach; ?>
                                    <?php else : ?>
                                        <li style="color: #d05151;">
                                            <img src="<?= BASE_URL ?>assets/img/alarm.svg" width="18">هنوز برنامه‌ای وجود ندارد.
                                        </li>
                                    <?php endif; ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="accTitles">
                        <div class="accTitle" data-tab="tomorrow">فردا</div>
                        <div class="accContents">
                            <div id="tomorrow" class="accContent">
                                <ul id="taskEntryTomorrow">
                                    <?php if (sizeof($tomorrowTasks) > 0) : ?>
                                        <?php foreach ($tomorrowTasks as $tomorrowTask) : ?>
                                            <div id="<?= $tomorrowTask->id ?>">
                                                <li data-taskId="<?= $tomorrowTask->id ?>" class="clickable <?= $tomorrowTask->is_done ? 'done' : '' ?>"><?= $tomorrowTask->is_done ? '<img src="' . BASE_URL . 'assets/img/checkbox.svg" width="20">' : '<img src="' . BASE_URL . 'assets/img/clock.svg" width="20">' ?><?= $tomorrowTask->title ?>
                                                    <span class="remove" title="حذف" data-taskId="<?= $tomorrowTask->id ?>">
                                                        <img src="<?= BASE_URL ?>assets/img/trash.svg" width="18">
                                                    </span></li>
                                            </div>
                                        <?php endforeach; ?>
                                    <?php else : ?>
                                        <li style="color: #d05151;">
                                            <img src="<?= BASE_URL ?>assets/img/alarm.svg" width="18">هنوز برنامه‌ای وجود ندارد.
                                        </li>
                                    <?php endif; ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="accTitles">
                        <div class="accTitle" data-tab="in2days"><?= $v->addDay(-6)->addDay(2)->format('l j F') ?></div>
                        <div class="accContents">
                            <div id="in2days" class="accContent">
                                <ul id="taskEntryin2days">
                                    <?php if (sizeof($in2daysTasks) > 0) : ?>
                                        <?php foreach ($in2daysTasks as $in2daysTask) : ?>
                                            <div id="<?= $in2daysTask->id ?>">
                                                <li data-taskId="<?= $in2daysTask->id ?>" class="clickable <?= $in2daysTask->is_done ? 'done' : '' ?>"><?= $in2daysTask->is_done ? '<img src="' . BASE_URL . 'assets/img/checkbox.svg" width="20">' : '<img src="' . BASE_URL . 'assets/img/clock.svg" width="20">' ?><?= $in2daysTask->title ?>
                                                    <span class="remove" title="حذف" data-taskId="<?= $in2daysTask->id ?>">
                                                        <img src="<?= BASE_URL ?>assets/img/trash.svg" width="18">
                                                    </span></li>
                                            </div>
                                        <?php endforeach; ?>
                                    <?php else : ?>
                                        <li style="color: #d05151;">
                                            <img src="<?= BASE_URL ?>assets/img/alarm.svg" width="18">هنوز برنامه‌ای وجود ندارد.
                                        </li>
                                    <?php endif; ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="accTitles">
                        <div class="accTitle" data-tab="in3days"><?= $v->addDay()->format('l j F') ?></div>
                        <div class="accContents">
                            <div id="in3days" class="accContent">
                                <ul id="taskEntryin3days">
                                    <?php if (sizeof($in3daysTasks) > 0) : ?>
                                        <?php foreach ($in3daysTasks as $in3daysTask) : ?>
                                            <div id="<?= $in3daysTask->id ?>">
                                                <li data-taskId="<?= $in3daysTask->id ?>" class="clickable <?= $in3daysTask->is_done ? 'done' : '' ?>"><?= $in3daysTask->is_done ? '<img src="' . BASE_URL . 'assets/img/checkbox.svg" width="20">' : '<img src="' . BASE_URL . 'assets/img/clock.svg" width="20">' ?><?= $in3daysTask->title ?>
                                                    <span class="remove" title="حذف" data-taskId="<?= $in3daysTask->id ?>">
                                                        <img src="<?= BASE_URL ?>assets/img/trash.svg" width="18">
                                                    </span></li>
                                            </div>
                                        <?php endforeach; ?>
                                    <?php else : ?>
                                        <li style="color: #d05151;">
                                            <img src="<?= BASE_URL ?>assets/img/alarm.svg" width="18">هنوز برنامه‌ای وجود ندارد.
                                        </li>
                                    <?php endif; ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="accTitles">
                        <div class="accTitle" data-tab="in4days"><?= $v->addDay()->format('l j F') ?></div>
                        <div class="accContents">
                            <div id="in4days" class="accContent">
                                <ul id="taskEntryin4days">
                                    <?php if (sizeof($in4daysTasks) > 0) : ?>
                                        <?php foreach ($in4daysTasks as $in4daysTask) : ?>
                                            <div id="<?= $in4daysTask->id ?>">
                                                <li data-taskId="<?= $in4daysTask->id ?>" class="clickable <?= $in4daysTask->is_done ? 'done' : '' ?>"><?= $in4daysTask->is_done ? '<img src="' . BASE_URL . 'assets/img/checkbox.svg" width="20">' : '<img src="' . BASE_URL . 'assets/img/clock.svg" width="20">' ?><?= $in4daysTask->title ?>
                                                    <span class="remove" title="حذف" data-taskId="<?= $in4daysTask->id ?>">
                                                        <img src="<?= BASE_URL ?>assets/img/trash.svg" width="18">
                                                    </span></li>
                                            </div>
                                        <?php endforeach; ?>
                                    <?php else : ?>
                                        <li style="color: #d05151;">
                                            <img src="<?= BASE_URL ?>assets/img/alarm.svg" width="18">هنوز برنامه‌ای وجود ندارد.
                                        </li>
                                    <?php endif; ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="accTitles">
                        <div class="accTitle" data-tab="in5days"><?= $v->addDay()->format('l j F') ?> </div>
                        <div class="accContents">
                            <div id="in5days" class="accContent">
                                <ul id="taskEntryin5days">
                                    <?php if (sizeof($in5daysTasks) > 0) : ?>
                                        <?php foreach ($in5daysTasks as $in5daysTask) : ?>
                                            <div id="<?= $in5daysTask->id ?>">
                                                <li data-taskId="<?= $in5daysTask->id ?>" class="clickable <?= $in5daysTask->is_done ? 'done' : '' ?>"><?= $in5daysTask->is_done ? '<img src="' . BASE_URL . 'assets/img/checkbox.svg" width="20">' : '<img src="' . BASE_URL . 'assets/img/clock.svg" width="20">' ?><?= $in5daysTask->title ?>
                                                    <span class="remove" title="حذف" data-taskId="<?= $in5daysTask->id ?>">
                                                        <img src="<?= BASE_URL ?>assets/img/trash.svg" width="18">
                                                    </span></li>
                                            </div>
                                        <?php endforeach; ?>
                                    <?php else : ?>
                                        <li style="color: #d05151;">
                                            <img src="<?= BASE_URL ?>assets/img/alarm.svg" width="18">هنوز برنامه‌ای وجود ندارد.
                                        </li>
                                    <?php endif; ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="accTitles">
                        <div class="accTitle" data-tab="in6days"><?= $v->addDay()->format('l j F') ?></div>
                        <div class="accContents">
                            <div id="in6days" class="accContent">
                                <ul id="taskEntryin6days">
                                    <?php if (sizeof($in6daysTasks) > 0) : ?>
                                        <?php foreach ($in6daysTasks as $in6daysTask) : ?>
                                            <div id="<?= $in6daysTask->id ?>">
                                                <li data-taskId="<?= $in6daysTask->id ?>" class="clickable <?= $in6daysTask->is_done ? 'done' : '' ?>"><?= $in6daysTask->is_done ? '<img src="' . BASE_URL . 'assets/img/checkbox.svg" width="20">' : '<img src="' . BASE_URL . 'assets/img/clock.svg" width="20">' ?><?= $in6daysTask->title ?>
                                                    <span class="remove" title="حذف" data-taskId="<?= $in6daysTask->id ?>">
                                                        <img src="<?= BASE_URL ?>assets/img/trash.svg" width="18">
                                                    </span></li>
                                            </div>
                                        <?php endforeach; ?>
                                    <?php else : ?>
                                        <li style="color: #d05151;">
                                            <img src="<?= BASE_URL ?>assets/img/alarm.svg" width="18">هنوز برنامه‌ای وجود ندارد.
                                        </li>
                                    <?php endif; ?>
                                </ul>
                            </div>
                        </div>
                    </div>

            </div>


            </section>
            <!-- End adding tabs -->
        </div>
    </div>
    </div>
    <footer>
        <div id="copyright">تمام حقوق این برنامه محفوظ و متعلق به سایت راز کد است.</div>
        <div id="programmer">برنامه ساز: علیرضا پورهادی</div>
    </footer>
    <ul id="goalsManagerM">
        <li><a href="<?= BASE_URL ?>primarylist.php" id="primarylist"><img src="<?= BASE_URL ?>assets/img/primarylist.svg" width="18">لیست اولیه</a></li>
        <li><a href="goalsManager.php"><img src="<?= BASE_URL ?>assets/img/goal.svg" width="16">مدیریت اهداف</a></li>

        <li><a href="<?= BASE_URL . 'setting.php' ?>" title="تنظیمات"><img src="<?= BASE_URL ?>assets/img/setting.svg" width="16">
                تنظیمات
            </a>
        </li>
        <li><a href="<?= BASE_URL . '?logout=1' ?>" title="خروج"><img src="<?= BASE_URL ?>assets/img/exit.svg" width="16">
                خروج
            </a>
        </li>
    </ul>
    <script src="<?=BASE_URL?>assets/js/jquery-3.5.1.min.js"></script>

    <script>
        $(document).ready(function() {
            $('.defaultTab').next().find('.default').slideDown();

            $('.searchIcon').click(function() {
                if ($(this).html() == '<img src="<?= BASE_URL ?>assets/img/close.png" width="22">') {
                    $(this).html('<img src="<?= BASE_URL ?>assets/img/search_icon.png" width="22">');
                    $('#search').animate({
                        opacity: 0
                    });
                } else {
                    $(this).html('<img src="<?= BASE_URL ?>assets/img/close.png" width="22">');
                    $('#search').animate({
                        opacity: 1
                    });
                }
            });

            $('#sidebar .addNewBtn').click(function(e) {
                e.preventDefault();
                var folderNameInput = $('#sidebar .addNew');
                $.ajax({
                    url: "process/ajaxHandler.php",
                    type: 'post',
                    data: {
                        action: "addFolder",
                        folderName: folderNameInput.val()
                    },
                    success: function(response) {
                        if (/^[\],:{}\s]*$/.test(response.replace(/\\["\\\/bfnrtu]/g, '@').replace(/"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g, ']').replace(/(?:^|:|,)(?:\s*\[)+/g, ''))) {
                            folderNameInput.val('');
                            var obj = JSON.parse(response);
                            $("#folders").append('<div id="' + obj.id + '"><a href="?folderId=' + obj.id + '"><li><img src="<?= BASE_URL ?>assets/img/cat.svg" width="14">' + obj.name + '</a><span class="remove" title="حذف" data-folderId="' + obj.id + '"><img src="<?= BASE_URL ?>assets/img/trashForCat.svg" width="18"></span></li></div>');
                        } else {
                            swal({
                                title: 'توجه',
                                text: response,
                                icon: "error",
                                className: 'bb',
                                button: {
                                    text: 'باشه',
                                    className: 'sab'
                                },
                            });
                        }
                    }
                });
            });

            $('div').on('click', '.accContents li', function(e) {
                e.stopPropagation();
                var obj = $(this);

                var tid = $(this).attr('data-taskId');
                $.ajax({
                    url: "process/ajaxHandler.php",
                    type: 'post',
                    data: {
                        action: "doneSwitch",
                        taskId: tid
                    },
                    success: function(response) {
                        if (response == 1) {
                            obj.removeClass();
                            obj.addClass('clickable done');
                            obj.find('img')[0].remove();
                            obj.prepend('<img src="<?= BASE_URL ?>assets/img/checkbox.svg" width="20">')
                        } else if (response == 0) {
                            obj.removeClass();
                            obj.addClass('clickable');
                            obj.find('img')[0].remove();
                            obj.prepend('<img src="<?= BASE_URL ?>assets/img/clock.svg" width="20">');
                        }
                    }
                });
            });

            $('#tasks .addNewBtn').click(function(e) {
                e.preventDefault();
                addNewTask();
            });

            $('#tasks .addNew').keydown(function(e) {
                if (e.keyCode === 13) {
                    addNewTask();
                }
            });

            function addNewTask() {
                var taskTitleInput = $('#tasks .addNew');
                var emroozCheckbox = $('#emrooz');
                var oneDayCheckbox = $('#1day');
                var twoDaysCheckbox = $('#2days');
                var threeDaysCheckbox = $('#3days');
                var fourDaysCheckbox = $('#4days');
                var fiveDaysCheckbox = $('#5days');
                var sixDaysCheckbox = $('#6days');
                var whatDay = ['none'];
                if (emroozCheckbox.is(":checked")) {
                    whatDay.push(emroozCheckbox.val());
                    emroozCheckbox.prop('checked', false);
                }

                if (oneDayCheckbox.is(":checked")) {
                    whatDay.push(oneDayCheckbox.val());
                    oneDayCheckbox.prop('checked', false);
                }

                if (twoDaysCheckbox.is(":checked")) {
                    whatDay.push(twoDaysCheckbox.val());
                    twoDaysCheckbox.prop('checked', false);
                }

                if (threeDaysCheckbox.is(":checked")) {
                    whatDay.push(threeDaysCheckbox.val());
                    threeDaysCheckbox.prop('checked', false);
                }

                if (fourDaysCheckbox.is(":checked")) {
                    whatDay.push(fourDaysCheckbox.val());
                    fourDaysCheckbox.prop('checked', false);
                }

                if (fiveDaysCheckbox.is(":checked")) {
                    whatDay.push(fiveDaysCheckbox.val());
                    fiveDaysCheckbox.prop('checked', false);
                }

                if (sixDaysCheckbox.is(":checked")) {
                    whatDay.push(sixDaysCheckbox.val());
                    sixDaysCheckbox.prop('checked', false);
                }

                if (whatDay.length > 1) {
                    whatDay.shift();
                }

                var taskBox = [];

                $.each(whatDay, function(index, value) {
                    if (value == 'emrooz') {
                        taskBox.push($('#taskEntryToday'));
                    } else if (value == '1day') {
                        taskBox.push($('#taskEntryTomorrow'));
                    } else if (value == '2days') {
                        taskBox.push($('#taskEntryin2days'));
                    } else if (value == '3days') {
                        taskBox.push($('#taskEntryin3days'));
                    } else if (value == '4days') {
                        taskBox.push($('#taskEntryin4days'));
                    } else if (value == '5days') {
                        taskBox.push($('#taskEntryin5days'));
                    } else if (value == '6days') {
                        taskBox.push($('#taskEntryin6days'));
                    }
                });

                $.ajax({
                    url: "process/ajaxHandler.php",
                    type: 'post',
                    data: {
                        action: "addTask",
                        dateSelects: whatDay,
                        taskTitle: taskTitleInput.val(),
                        folderId: <?= isset($_GET['folderId']) ? $_GET['folderId'] : '"all"' ?>
                    },
                    success: function(response) {

                        if (/^[\],:{}\s]*$/.test(response.replace(/\\["\\\/bfnrtu]/g, '@').replace(/"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g, ']').replace(/(?:^|:|,)(?:\s*\[)+/g, ''))) {
                            taskTitleInput.val('');
                            var obj = JSON.parse(response);
                            $.each(obj, function(index, value) {
                                if (taskBox[index].text().trim() == 'هنوز برنامه‌ای وجود ندارد.') {
                                    taskBox[index].empty();
                                }
                                taskBox[index].append('<div id="' + value.id + '"><li data-taskId="' + value.id + '" class="clickable">' + value.title + '<span data-taskId="' + value.id + '" class="remove" title="حذف" ><img src="<?= BASE_URL ?>assets/img/trash.svg" width="18"></span></li></div>');
                            });

                        } else {
                            swal({
                                title: 'توجه',
                                text: response,
                                icon: "error",
                                className: 'bb',
                                button: {
                                    text: 'باشه',
                                    className: 'sab'
                                },
                            });
                        }
                    }
                });
            }

            $('div').on('click', '#folders .remove', function(e) {
                e.stopPropagation();
                var folderName = $(this).parent().text();
                folderName = folderName.trim();

                swal({
                        title: 'توجه',
                        text: 'آیا مطمئن هستید؟',
                        icon: 'warning',
                        className: "DBox",
                        buttons: {
                            cancel: {
                                visible: true,
                                text: "لغو",
                                className: 'cancelb'
                            },
                            confirm: {
                                text: 'تایید',
                                className: 'confirmb'
                            }
                        },
                        dangerMode: true,
                    })
                    .then((willDelete) => {
                        if (willDelete) {
                            var fid = $(this).attr('data-folderId');
                            $.ajax({
                                url: "process/ajaxHandler.php",
                                type: 'post',
                                data: {
                                    action: "deleteFolder",
                                    folderId: fid
                                },
                                success: function(response) {
                                    $('#folders .remove').parents('#folders').children("div[id|='" + fid + "']").fadeOut(700);
                                }
                            });
                        }
                    });
            });

            $('div').on('click', '.accContents .remove', function(e) {
                e.stopPropagation();
                var obj = $(this);
                var parentObjId = obj.parents("[id^='taskEntry']").attr('id');
                var taskTitle = obj.parent().text();
                taskTitle = taskTitle.trim();
                swal({
                        title: 'توجه',
                        text: 'آیا مطمئن هستید؟',
                        icon: 'warning',
                        className: "DBox",
                        buttons: {
                            cancel: {
                                visible: true,
                                text: "لغو",
                                className: 'cancelb'
                            },
                            confirm: {
                                text: 'تایید',
                                className: 'confirmb'
                            }
                        },
                        dangerMode: true,
                    })
                    .then((willDelete) => {
                        if (willDelete) {
                            var tid = obj.attr('data-taskId');
                            $.ajax({
                                url: "process/ajaxHandler.php",
                                type: 'post',
                                data: {
                                    action: "deleteTask",
                                    taskId: tid
                                },
                                success: function(response) {
                                    obj.parents('.accContents').find("div[id|='" + tid + "']").remove();
                                    noProgram(parentObjId);
                                }
                            });
                        }
                    });
            });

            function noProgram(id) {
                if ($('#' + id).children().length == 0) {
                    $('#' + id).html('<li style="color: #d05151;"><img src="<?= BASE_URL ?>assets/img/alarm.svg" width="18">هنوز برنامه‌ای وجود ندارد.</li>');
                }
            }

            $('#search button').click(function(e) {
                e.preventDefault();
                doSearch(1);
            });

            $('#search button').keydown(function(e) {
                if (e.keyCode === 13) {
                    doSearch(1);
                }
            });

            function doSearch(page) {
                var searchEntry = $('#search #psearch').val();
                $.ajax({
                    url: "process/ajaxHandler.php",
                    type: 'post',
                    data: {
                        action: "doSearch",
                        page: page,
                        searchEntry: searchEntry
                    },
                    success: function(response) {
                        $('#ss').parent().parent().find('.accContent').slideUp();
                        if ($('#ss').next().find('.accContent').css('display') == 'none') {
                            $('#ss').slideDown();
                            $('#ss').next().find('.accContent').slideDown();
                        }
                        if (/^[\],:{}\s]*$/.test(response.replace(/\\["\\\/bfnrtu]/g, '@').replace(/"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g, ']').replace(/(?:^|:|,)(?:\s*\[)+/g, ''))) {
                            var obj = JSON.parse(response);
                            $('#searchResults ul').empty('div, li')
                            $.each(obj, function(ind, val) {
                                if (ind != 'numberOfPages') {
                                    var today = new Date(val.created_at).toLocaleDateString('fa-IR');
                                    if (today == '۱۳۶۰/۳/۲۴') {
                                        today = 'لیست اولیه';
                                    }
                                    $('#searchResults ul').append('<div id="' + val.id + '"><li data-taskId="' + val.id + '" class="clickable">' + val.title + '<span class="searchDate">' + today + '</span><span data-taskId="' + val.id + '" class="remove" title="حذف" ><img src="<?= BASE_URL ?>assets/img/trash.svg" width="18"></span></li></div>');
                                } else {
                                    if (val == 0) {
                                        $('#searchResults ul').append('<li style="color: #d05151;">موردی یافت نشد.</li>');
                                    }
                                    var pageNumber = getPageNumber(val);
                                    var baseUrl = "<?= BASE_URL ?>";
                                    $('#searchResults #pagination').empty();
                                    $('#searchResults #pagination').append("<br><br><br><br><span data-page='" + pageNumber + "'>»</span>");

                                    for (let i = 1; i <= pageNumber; i++) {

                                        if (i == page) {
                                            $('#searchResults #pagination').append('<strong>' + i + '</strong>');
                                        } else {
                                            $('#searchResults #pagination').append('<span data-page="' + i + '">' + i + '</span>');
                                        }

                                    }
                                    $('#searchResults #pagination').append("<span data-page='1'>«</span><br><br><br>");
                                }
                            })

                        } else {
                            swal({
                                title: 'توجه',
                                text: response,
                                icon: "error",
                                className: 'bb',
                                button: {
                                    text: 'باشه',
                                    className: 'sab'
                                },
                            });
                        }
                    }
                });
            }

            function getPageNumber(val) {
                var tasksPerPage = <?= TASKS_PER_PAGE ?>;
                var pageNumbers = Math.ceil(Number(val) / Number(tasksPerPage));
                return pageNumbers;
            }


            $('div').on('click', '#searchResults #pagination span', function(e) {
                e.stopPropagation();
                doSearch($(this).attr('data-page'));
            });


            $('.accTitle').click(function() {
                $(this).parent().parent().find('.accContent').slideUp();
                if ($(this).next().find('.accContent').css('display') == 'none') {
                    $(this).next().find('.accContent').slideDown();
                }
            });

        });
    </script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
</body>

</html>