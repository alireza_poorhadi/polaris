<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="<?= BASE_URL ?>assets/img/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="<?= BASE_URL ?>assets/css/baha1.css">
    <title><?= SITE_TITLE ?></title>
    <script src="<?= BASE_URL ?>assets/js/jquery-3.5.1.min.js"></script>
</head>

<body>
    <div id="container">
        <div id="guide">در این قسمت می‌توانید بهایی را که حاضرید پرداخت کنید تا به هدف مورد نظر خود برسید، ذکر نمایید.</div>
        <a class="more">توضیحات بیشتر ...</a>
        <div id="moreExp" cite="https://abasmanesh.com/fa/">
            <p>افراد زیادی وقتی از نقش باورهای خود در نتایج زندگی شان مطلع می شوند، به دنبال روشی جادویی برای تغییر باورهای شان هستند <span style="color: red;">زیرا نمی خواهند بهای موفقیت شان را بپردازند.</span></p>

            <p>مگر می شود عاشق نویسندگی باشی اما از تمرکز و تلاش برای مهارت بیشتر در این باره، لذت نبری!</p>

            <p>مگر می شود عاشق فوتبال باشی، اما وقت گذراندن برای تمرینات برایت زجر آور باشد؟!</p>

            <p>فردی که عاشق نویسندگی، فوتبال و… است، لذت بخش ترین لحظات زندگی اش را زمانی می داند که صرف تمرین و تلاش برای کسب مهارت در بهتر نوشتن یا بهتر ضربه زدن به توپ می گردد.</p>

            <p>وقتی این حد از عشق و تمرکز، با باورهای قدرتمند کننده ی هماهنگ با آن خواسته همراه گردد، فرد را به مسیر راهکارها، ایده ها و الهاماتی هدایت می کند که این علاقه و تمرکز به ثمر نشسته و از او فوتبالیستی ماهر یا نویسنده ای با کتابهای پرفروش می سازد.
            </p>

            <p style="color: green;"> پس اگر هدفی داری اما علاقه ای به پرداخت بهایش نداری، یعنی هنوز ایمان و عشق واقعی نسبت به آن هدف نداری.</p>

            <p> یعنی آن هدف آنقدرها برایت جدی نیست.</p>

            <p>و تا زمانی که چنین حدی از ایمان، علاقه و تعهد برای پرداخت بهای اهداف در وجودت شکل نگیرد، راهی به سمت ساختن باورهای قدرتمند کننده و هدایت به مدار خواسته ات نداری.</p>

            <p>نتایج تو زمانی تغییر می کند که مدارت تغییر کند. تنها زمانی به خواسته ات می رسی که باورهای هماهنگ با خواسته ات را بسازی.</p>

            <p>اما منظور از باور ساختن، نشستن در یک گوشه و تجسم برای پدیدار شدن هدف نیست. بلکه هر تلاش ذهنی ای که صرف کنترل ورودی های ذهن می‌شود به گونه‌ای که به تو احساس اطمینان‌، قدرت و آرامش بیشتری ببخشد‌، بخشی از مسیر هدایت تو به سمت اقدامات یا اجرای ایده‌هایی است که موجب می‌شود وارد مدار خواسته‌ات شوی و با آن ملاقات کنی.</p>

            <p> باور ساختن چیزی جدا نیست از: اقداماتی عملی‌ی هم اساس و هم جهت با آن باورها.</p>

            <p>نتیجه باور، همیشه یک اقدام عملی است. باور همیشه و بدون استثناء به یک اقدام عملی ختم می شود.</p>

            <p>اگر ایمان و باور شما منجر به اقدام عملی در آن مسیر نمی گردد، یعنی اصلا ایمانی وجود ندارد و باوری ساخته نشده است. برای همین است که در قرآن‌، ایمان و عمل صالح(اقدامات هم اساس با باورهای قدرتمندکننده) همواره در کنار هم آمده‌اند.</p>

            <p>اقدامات شما همیشه به اندازه باورهایی است که ساخته اید. اگر اقدامات‌تان سطحی و غیر جدی است، یعنی باورهای‌تان سطحی و ایمان‌تان همان حرف مفتی است که عمل نیاورده است.</p>

            <p>اما مشکل اینجاست که افراد میوه باورها را می خواهند‌، بدون اینکه قصد پرداخت بهای ساخته شدن ریشه ها و جسارت تغییر رویه‌شان را داشته باشند و چنین</p>

            <p>چیزی غیر ممکن است. برای همین آدمهای کمی موفق به ساختن باورهای قدرتمند کننده می گردند.</p>

            <p> باور ساختن‌‌، از یک ایمان شکست ناپذیر و عشق وافر برای حرکت در مسیر اهداف شروع می شود. سپس با عمل به ایده هایی که در هر مرحله از این مسیر به شما گفته می شود، کم کم ریشه باورهای قدرتمند کننده در وجودتان شکل می گیرد و با هر اقدام بعدی، کم کم جوانه زده و تبدیل به درختی پرثمر با ریشه هایی عمیق می گردد که خود را در قالب، ایمان راسخ تر، جسارت و اعتماد به نفس بیشتر برای برداشتن قدم های محکم تر و نیز نتایجی نشان می دهد که هر بار بهتر و بهتر می گردد.</p>

            <p>آنچه شما را در مدار خواسته ات ثابت قدم نگه می دارد و به موفقیت هایت دوام و ثبات می بخشد، باورهای ریشه داری است که بهای رشد آنها را به کمک وفادار ماندن به اصولی پرداخته ای که در ابتدای راه برای خود در نظر گرفته ای</p>

            <p>اصولی که وفاداری به آن موجب می گردد فارغ از اینکه دیگران چه روشی دارند، یا شرایط چقدر سخت شده، یا نجواهای ذهنت نزدیک است گمراهت نماید، تو را وادار به حفظ ایمان و ماندن در مسیر صحیح می نماید.</p>

            <p>“تعیین اصول در ابتدای راه و وفاداری به اصول در تمام طول مسیر”، شیوه ای جادویی است که موجب ایجاد نتایجی باورنکردنی می گردد.</p>

            <p>می خواهم بگویم خداوند پاسخ می دهد اگر ایمانت را نشان دهی، به اصولت وفادار بمانی و بهای خواسته ات را بپردازی.</p>

            <p style="background-color: lightgreen; padding: 0 7px; border-radius: 3px; text-align:center;">زیرا بهشت را به بها دهند و نه به بهانه!</p>
            <br>
            <p style="font-size: 16px;">منبع: <a href="https://abasmanesh.com/fa/" title="گروه تحقیقاتی عباس‌منش" target="_blank" style="color: blue;">گروه تحقیقاتی عباس‌منش</a></p>
            <br>
            <hr>

        </div>



        <h2>برای رسیدن به این هدف حاضرم که بهای زیر را پرداخت کنم:</h2>
        <h3><?= $goal->content; ?></h3>
        <div id="bahaInput">
            <input type="text" placeholder="افزودن">
        </div>

        <ul>
            <?php if (sizeof($bahas) > 0) : ?>
                <?php foreach ($bahas as $baha) : ?>
                    <div id="<?= $baha->id ?>">
                        <li data-yearBahaId="<?= $baha->id ?>"><?= $baha->content ?>
                            <span class="remove .tooltip" title="حذف" data-yearBahaId="<?= $baha->id ?>">
                                <img src="<?= BASE_URL ?>assets/img/trash.svg" width="18">
                            </span></li>
                    </div>
                <?php endforeach; ?>
            <?php else : ?>
                <li style="color: #d05151; list-style-type: none;">موردی وجود ندارد.</li>
            <?php endif; ?>
        </ul>
        <br><br>
        <div style="text-align: center;">
            <a href="<?= BASE_URL . 'goalsManager.php' ?>" id="return">بازگشت</a>
        </div>

    </div>


    <script>
        $(document).ready(function() {
            $('.more').click(function() {
                $('#moreExp').slideToggle(1000);
                if ($('.more').text() == 'توضیحات بیشتر ...') {
                    $('.more').text('حذف توضیحات');
                } else {
                    $('.more').text('توضیحات بیشتر ...');
                }

            });

            // adding new baha (ajax)

            // $('#bahaInput button').click(function(e) {
            //     e.preventDefault();
            //     addNewGoal();
            // });

            $('#bahaInput input').keydown(function(e) {
                if (e.keyCode === 13) {
                    addNewBaha();
                }
            });

            function addNewBaha() {
                var newYearBahaVal = $('#bahaInput input').val();
                $.ajax({
                    url: "process/ajaxHandler.php",
                    type: 'post',
                    data: {
                        action: "addBaha",
                        newYearBaha: newYearBahaVal,
                        yearGoalId: <?= isset($_GET['yearGoalId']) ? ($_GET['yearGoalId']) : null; ?>
                    },
                    success: function(response) {

                        if (/^[\],:{}\s]*$/.test(response.replace(/\\["\\\/bfnrtu]/g, '@').replace(/"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g, ']').replace(/(?:^|:|,)(?:\s*\[)+/g, ''))) {
                            if ($('#container ul li').text().trim() == 'موردی وجود ندارد.') {
                                $('#container ul li').remove();
                            }
                            $('#bahaInput input').val('');
                            var obj = JSON.parse(response);
                            $('#container ul').append('<div id="' + obj.id + '"><li data-yearBahaId="' + obj.id + '">' + obj.content + '<span data-yearBahaId="' + obj.id + '" class="remove" title="حذف" ><img src="<?= BASE_URL ?>assets/img/trash.svg" width="18"></span></li></div>');
                        } else {
                            swal({
                                text: response,
                                icon: "error",
                                className: 'bb',
                                button: {
                                    text: 'باشه',
                                    className: 'sab'
                                },
                            });
                        }
                    }

                });
            }

            // deleting year baha

            $('body').on('click', '#container ul .remove', function(e) {
                var obj = $(this);
                var parentObj = obj.parents("ul");
                swal({
                        title: 'توجه',
                        text: 'آیا مطمئن هستید؟',
                        icon: 'warning',
                        className: "DBox",
                        buttons: {
                            cancel: {
                                visible: true,
                                text: "لغو",
                                className: 'cancelb'
                            },
                            confirm: {
                                text: 'تایید',
                                className: 'confirmb'
                            }
                        },
                        dangerMode: true,
                    })
                    .then((willDelete) => {
                        if (willDelete) {
                            var ybid = obj.parent().attr('data-yearBahaId');
                            $.ajax({
                                url: "process/ajaxHandler.php",
                                type: 'post',
                                data: {
                                    action: "deleteYearBaha",
                                    yearBahaId: ybid
                                },
                                success: function(response) {
                                    obj.parents('ul').find("[id='" + ybid + "']").fadeOut(1000, function() {
                                        obj.parents('ul').find("[id='" + ybid + "']").remove();
                                    });
                                    noProgramForYearBaha(parentObj);

                                }
                            });
                        }
                    });
            });

            function noProgramForYearBaha(parentObj) {
                if (parentObj.children().length == 1) {
                    parentObj.html('<li style="color: #d05151; list-style-type: none;">موردی وجود ندارد.</li>');
                }
            }
        });
    </script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script>
        $(".tooltip , span").slTooltip({
            width: 50
        });
    </script>


</body>

</html>