<?php
session_start();
date_default_timezone_set('Asia/Tehran');

include_once 'constants.php';
include_once BASE_PATH . 'bootstrap/config.php';
include_once BASE_PATH . 'vendor/autoload.php';
include_once BASE_PATH . 'libs/helpers.php';


try {
    $conn = new PDO("mysql:host=$host;dbname=$database;charset=utf8mb4", $user, $password);
} catch (PDOException $e) {
    diePage("failed to connect to database !" . $e->getMessage());
}

use Hekmatinasser\Verta\Verta;
$v = new Verta();
$currentYear = $v->format('Y');
$currentMonth = $v->format('F');
$currentTime = $v->format('H');

include_once BASE_PATH . 'libs/auth.php';
include_once BASE_PATH . 'libs/tasks.php';
include_once BASE_PATH . 'libs/lib-goals.php';
include_once BASE_PATH . 'libs/lib-gs.php';
