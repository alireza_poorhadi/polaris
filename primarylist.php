<?php

include_once 'bootstrap/init.php';
if (isset($_GET['logout'])) {
    doLogout();
}

if (!isloggedIn()) {
    header("location:" . BASE_URL . "auth.php");
}

$primaryLists = getPrimaryList();
$folders = getFolders();

include_once 'view/view-primarylist.php';