<?php
include_once 'bootstrap/init.php';

$msg = null;
$error = false;
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $action = $_GET['action'];
    $params = $_POST;
    if ($action == 'register') {
        if (!mb_check_encoding($params['email'], 'ASCII')) {
            $msg = "لطفاً فقط از کاراکترهای لاتین برای فیلد نام کاربری یا ایمیل استفاده کنید";
            $error = true;
        }
        if (!$error) {
            $result = doRegister($params);
            if ($result == false) {
                $msg = 'متاسفانه در روند ثبت‌نام شما مشکلی بوجود آمد';
                $error = true;
            } else {
                $msg = 'ثبت‌نام شما با موفقیت انجام شد. اکنون می‌توانید وارد سامانه شوید';
            }
        }
    } else if ($action == 'login') {
        $rememberMe = (isset($_POST['rememberMe'])) ? $_POST['rememberMe'] : 0;
        $result = doLogin($params['email'], $params['password'], $rememberMe);
        if (!$result) {
            $msg = 'ایمیل / نام کاربری یا رمز عبور نادرست است';
            $error = true;
        } else {
            $msg = 'شما با موفقیت وارد سامانه شدید';
            header('Refresh: 1; ' . BASE_URL);
        }
    }
}

include_once 'view/view-auth.php';
