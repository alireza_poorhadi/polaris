<?php

include_once 'bootstrap/init.php';
if (isset($_GET['logout'])) {
    doLogout();
}

if (!isloggedIn()) {
    header("location:" . BASE_URL . "auth.php");
}


if(isset($_GET['yearGoalId']) and is_numeric($_GET['yearGoalId'])) {
    $yearGoalId = $_GET['yearGoalId'];
}
$bahas = getBahas($yearGoalId);
$goal = getGoal($yearGoalId);

include_once 'view/view-baha.php';