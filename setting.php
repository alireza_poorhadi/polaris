<?php

include_once 'bootstrap/init.php';
if (isset($_GET['logout'])) {
    doLogout();
}

if (!isloggedIn()) {
    header("location:" . BASE_URL . "auth.php");
}

date_default_timezone_set('Asia/Tehran');

$userId = getCurrentUserId();
$u = getUsername($userId);
$username = $u->fullname;
$userEmail = $u->email;
$allUsers = getAllUsers();

include_once 'view/view-setting.php';