<?php
include_once '../bootstrap/init.php';

if (!isAjaxRequest()) {
    diePage("Invalid Request!");
}

if (!isset($_POST['action']) or empty($_POST['action'])) {
    diePage("Invalid Action!");
}

switch ($_POST['action']) {
    case 'addFolder':
        $folderName = $_POST['folderName'];
        if (empty($folderName) or !isset($folderName) or mb_strlen($folderName) < 2) {
            echo "نام دسته‌بندی باید بیشتر از دو حرف باشد!";
            die();
        }
        $res = addNewFolder($folderName);
        echo json_encode($res);
        break;

    case 'addTask':
        $taskTitle = $_POST['taskTitle'];
        $folderId = $_POST['folderId'];
        $dateSelects = $_POST['dateSelects'];
        if (empty($taskTitle) or !isset($taskTitle)) {
            echo "نام برنامه باید بیشتر از سه حرف باشد!";
            die();
        }
        if ($folderId == 'all') {
            die('لطفاً دسته‌بندی مد نظر خود را انتخاب کنید.');
        }

        if ($dateSelects == ['none']) {
            die('لطفاً روز مد نظر خود را انتخاب کنید.');
        }

        foreach ($dateSelects as $dateSelect) {
            $res[] = addNewTask($taskTitle, $folderId, $dateSelect);
        }

        echo json_encode($res);
        break;

    case 'doneSwitch':
        $taskId = $_POST['taskId'];
        if (!isset($taskId) or !is_numeric($taskId)) {
            die("خطای غیر‌منتظره ...!");
        }
        echo changeTaskStatus($taskId);
        break;

    case 'checkEmailExistence':
        $email = $_POST['email'];
        $result = getUserByEmail($email);
        echo is_null($result) ? true : false;
        break;

    case 'deleteFolder':
        $folderId = $_POST['folderId'];
        deleteFolder($folderId);
        break;

    case 'deleteTask';
        $taskId = $_POST['taskId'];
        deleteTask($taskId);
        break;

    case 'doSearch':
        $searchEntry = $_POST['searchEntry'];
        $page = $_POST['page'];
        if (empty($searchEntry)) {
            echo "لطفا برنامه مورد نظر خود را برای جستجو وارد کنید.";
            die();
        }
        $res = doSearch($searchEntry, $page);
        echo json_encode($res);
        break;

    case 'deleteYearGoal':
        $yearGoalId = $_POST['yearGoalId'];
        deleteYearGoal($yearGoalId);
        break;

    case 'yearGoalDoneSwitch':
        $yearGoalId = $_POST['yearGoalId'];
        if (!isset($yearGoalId) or !is_numeric($yearGoalId)) {
            die("خطای غیر‌منتظره ...!");
        }
        echo changeYearGoalStatus($yearGoalId);
        break;

    case 'addGoal':
        $taskTitle = $_POST['taskTitle'];
        $dateSelect = $_POST['dateSelect'];
        if (empty($taskTitle) or !isset($taskTitle) or mb_strlen($taskTitle) < 4) {
            echo "نام هدف باید بیشتر از سه حرف باشد!";
            die();
        }
        $res = addNewGoal($taskTitle, $dateSelect);
        echo json_encode($res);
        break;

    case 'showYearGoals':
        $whichYear = $_POST['whichYear'];
        $res = getYearGoals($whichYear);
        echo json_encode($res);
        break;

    case 'getMonthGoals':
        $whichMonth = $_POST['whichMonth'];
        $folderId = $_POST['folderId'];
        $res = getMonthGoals($whichMonth, $folderId);
        echo json_encode($res);
        break;

    case 'getDecisions':
        $whichMonth = $_POST['whichMonth'];
        $folderId = $_POST['folderId'];
        $res = getDecisions($whichMonth, $folderId);
        echo json_encode($res);
        break;

    case 'getAssessments':
        $whichMonth = $_POST['whichMonth'];
        $folderId = $_POST['folderId'];
        $res = getAssessments($whichMonth, $folderId);
        echo json_encode($res);
        break;

    case 'addNewMonthGoal':
        $whichMonth = $_POST['whichMonth'];
        $newMonthGoal = $_POST['newMonthGoal'];
        $folderId = $_POST['folderId'];
        if (empty($newMonthGoal)) {
            die('لطفا هدف خود را وارد کنید!');
        }
        if ($folderId == 'all') {
            die('لطفاً دسته‌بندی مورد نظر خود را انتخاب کنید!');
        }
        $res = addMonthGoal($newMonthGoal, $whichMonth, $folderId);
        echo json_encode($res);
        break;

    case 'addNewDecision':
        $whichMonth = $_POST['whichMonth'];
        $newDecision = $_POST['newDecision'];
        $folderId = $_POST['folderId'];
        if (empty($newDecision)) {
            die('لطفا تصمیم خود را وارد کنید!');
        }
        if ($folderId == 'all') {
            die('لطفاً دسته‌بندی مورد نظر خود را انتخاب کنید!');
        }
        $res = addDecision($newDecision, $whichMonth, $folderId);
        echo json_encode($res);
        break;

    case 'addNewAssessment':
        $whichMonth = $_POST['whichMonth'];
        $newAssessment = $_POST['newAssessment'];
        $folderId = $_POST['folderId'];
        if (empty($newAssessment)) {
            die('لطفا ارزیابی خود را وارد کنید!');
        }
        if ($folderId == 'all') {
            die('لطفاً دسته‌بندی مورد نظر خود را انتخاب کنید!');
        }
        $res = addAssessment($newAssessment, $whichMonth, $folderId);
        echo json_encode($res);
        break;

    case 'deleteMonthGoal':
        $monthGoalId = $_POST['monthGoalId'];
        deleteMonthGoal($monthGoalId);
        break;

    case 'deleteDecision':
        $decisionId = $_POST['decisionId'];
        deleteDecision($decisionId);
        break;

    case 'deleteAssessment':
        $assessmentId = $_POST['assessmentId'];
        deleteAssessment($assessmentId);
        break;

    case 'monthGoalDoneSwitch':
        $monthGoalId = $_POST['monthGoalId'];
        if (!isset($monthGoalId) or !is_numeric($monthGoalId)) {
            die("خطای غیر‌منتظره ...!");
        }
        echo changeMonthGoalStatus($monthGoalId);
        break;

    case 'decisionDoneSwitch':
        $decisionId = $_POST['decisionId'];
        if (!isset($decisionId) or !is_numeric($decisionId)) {
            die("خطای غیر‌منتظره ...!");
        }
        echo changeDecisionStatus($decisionId);
        break;

    case 'doSearchForGoals':
        $searchEntry = $_POST['searchEntry'];
        $page = $_POST['page'];
        if (empty($searchEntry)) {
            echo "لطفا عبارت مورد نظر خود را برای جستجو وارد کنید.";
            die();
        }
        $res = doSearchForGoals($searchEntry, $page);
        echo json_encode($res);
        break;

    case 'doSearchForDecisions':
        $searchEntry = $_POST['searchEntry'];
        $page = $_POST['page'];
        if (empty($searchEntry)) {
            echo "لطفا عبارت مورد نظر خود را برای جستجو وارد کنید.";
            die();
        }
        $res = doSearchForDecisions($searchEntry, $page);
        echo json_encode($res);
        break;

    case 'doSearchForAssessments':
        $searchEntry = $_POST['searchEntry'];
        $page = $_POST['page'];
        if (empty($searchEntry)) {
            echo "لطفا عبارت مورد نظر خود را برای جستجو وارد کنید.";
            die();
        }
        $res = doSearchForAssessments($searchEntry, $page);
        echo json_encode($res);
        break;

    case 'addBaha':
        $newYearBaha = $_POST['newYearBaha'];
        $yearGoalId = $_POST['yearGoalId'];
        if (empty($newYearBaha)) {
            echo "شما هنوز چیزی وارد نکرده‌اید!";
            die();
        }
        if (is_null($yearGoalId)) {
            echo "شما هدفی انتخاب نکرده‌اید!";
        }
        $res = addNewYearBaha($newYearBaha, $yearGoalId);
        echo json_encode($res);
        break;

    case 'addNReason':
        $newYearNReason = $_POST['newYearNReason'];
        $yearGoalId = $_POST['yearGoalId'];
        if (empty($newYearNReason)) {
            echo "شما هنوز چیزی وارد نکرده‌اید!";
            die();
        }
        if (is_null($yearGoalId)) {
            echo "شما هدفی انتخاب نکرده‌اید!";
        }
        $res = addNewYearNReason($newYearNReason, $yearGoalId);
        echo json_encode($res);
        break;

    case 'addPReason':
        $newYearPReason = $_POST['newYearPReason'];
        $yearGoalId = $_POST['yearGoalId'];
        if (empty($newYearPReason)) {
            echo "شما هنوز چیزی وارد نکرده‌اید!";
            die();
        }
        if (is_null($yearGoalId)) {
            echo "شما هدفی انتخاب نکرده‌اید!";
        }
        $res = addNewYearPReason($newYearPReason, $yearGoalId);
        echo json_encode($res);
        break;

    case 'deleteYearBaha':
        $yearBahaId = $_POST['yearBahaId'];
        deleteYearBaha($yearBahaId);
        break;

    case 'deleteYearNReason':
        $yearNReasonId = $_POST['yearNReasonId'];
        deleteYearNReason($yearNReasonId);
        break;

    case 'deleteYearPReason':
        $yearPReasonId = $_POST['yearPReasonId'];
        deleteYearPReason($yearPReasonId);
        break;

    case 'addTaskPL':
        $newTaskPL = $_POST['newTaskPL'];
        if (empty($newTaskPL)) {
            echo "شما هنوز چیزی وارد نکرده‌اید!";
            die();
        }
        $res = addNewTaskPL($newTaskPL);
        echo json_encode($res);
        break;

    case 'addTaskFromPL':
        $folderId = $_POST['folderId'];
        $taskId = $_POST['taskId'];
        $dateSelects = $_POST['dateSelects'];
        $taskTitle = $_POST['taskTitle'];

        if ($dateSelects == ['none']) {
            die('لطفاً روز مد نظر خود را انتخاب کنید.');
        }


        foreach ($dateSelects as $ind => $dateSelect) {
            if ($ind == 0) {
                updateTaskFromPL($taskId, $folderId, $dateSelect);
            } else {
                addNewTask($taskTitle, $folderId, $dateSelect);
            }
        }

        echo "OK";

        break;

    default:
        diePage("Invalid Action!");
}
