<?php
include_once '../bootstrap/init.php';

if (!isAjaxRequest()) {
    diePage("Invalid Request!");
}

$newUsername = $_POST['newUsername'];
$oldPass = $_POST['oldPass'];
$newPass = $_POST['newPass'];
$reNewPass = $_POST['reNewPass'];

if(!empty($oldPass)) {
    if(empty($newPass) and empty($reNewPass)) {
        echo "لطفاً رمز عبور جدید را وارد کنید.";
    } else if($newPass !== $reNewPass) {
        echo "رمزهای جدید وارد شده مطابقت ندارند.";
    } else {
        changePass($oldPass, $newPass, $reNewPass);
    }
    
}
changeUsername($newUsername);
